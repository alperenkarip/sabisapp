import "./i18n"
import "./utils/ignore-warnings"
import React, { useEffect } from "react"
import { SafeAreaProvider, initialWindowMetrics } from "react-native-safe-area-context"
import { initFonts } from "./theme/fonts" // expo
import { Platform, View, Text } from "react-native"
import * as storage from "./utils/storage"
import { useBackButtonHandler, AppNavigator, canExit, useNavigationPersistence } from "./navigators"
// import { RootStore, RootStoreProvider, setupRootStore } from "./models"
import { ErrorBoundary } from "./screens/error/error-boundary"
import { notificationManager } from "./components/notification-manager/notification-manager"
import { Observer } from "mobx-react-lite"
import UserStore from "./services/stores/UserStore"
import { configure } from "mobx"
import { TextLoader, DoubleCircleLoader } from "react-native-indicator"
import AppStore from "./services/stores/AppStore"
import { Provider as PaperProvider } from "react-native-paper"
import i18n from "./i18n"
import { notifications } from "./components/notification-manager/notifications"

const initI18n = i18n

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

function App() {
  configure({
    useProxies: "never",
  })

  useBackButtonHandler(canExit)
  const {
    initialNavigationState,
    onNavigationStateChange,
    isRestored: isNavigationStateRestored,
  } = useNavigationPersistence(storage, NAVIGATION_PERSISTENCE_KEY)
  useEffect(() => {
    notificationManager.getToken()
    notificationManager.listenNotification()
  }, [])

  useEffect(() => {
    ;(async () => {
      await initFonts() // expo
      if (Platform.OS === "ios") {
        notificationManager.registerForRemoteMessages()
      }
    })()
  }, [])

  useEffect(() => {
    storage.load("users").then((data) => {
      if (!data) {
        storage.save("users", [])
        console.log("users storage oluşturuldu")
      }
    })
  }, [])

  useEffect(() => {
    setTimeout(() => {
      AppStore.setLoading(false)
    }, 1000)
  }, [])

  return (
    <SafeAreaProvider initialMetrics={initialWindowMetrics}>
      <ErrorBoundary catchErrors={"always"}>
        <Observer>
          {() => {
            return (
              <PaperProvider theme={AppStore.getTheme()}>
                {/* AppStore.getTheme, component re-render için gerekli */}
                <Text style={{ display: "none" }}>{AppStore.activeTheme}</Text>
                <View style={{ flex: 1 }}>
                  {AppStore.loading ? (
                    <View
                      style={{
                        position: "absolute",
                        // backgroundColor: "#000",
                        flex: 1,
                        zIndex: 999,
                        width: "100%",
                        height: "100%",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <View
                        style={{
                          position: "absolute",
                          backgroundColor: "#000",
                          width: "100%",
                          height: "100%",
                          opacity: 0.5,
                        }}
                      ></View>
                      <TextLoader text="Yükleniyor" textStyle={{ fontSize: 16, color: "#fff" }} />
                      {/* <DoubleCircleLoader color={color.white} size={40} /> */}
                    </View>
                  ) : null}
                  <AppNavigator
                    initialState={initialNavigationState}
                    onStateChange={onNavigationStateChange}
                    isAuth={UserStore.accessToken}
                  />
                </View>
              </PaperProvider>
            )
          }}
        </Observer>
      </ErrorBoundary>
    </SafeAreaProvider>
  )
}
declare global {
  namespace ReactNativePaper {
    interface ThemeColors {
      subTextColor: string
      third: string
    }

    interface Theme {
      subTextColor: boolean
      third: boolean
    }
  }
}

export default App
