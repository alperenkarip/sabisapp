import * as React from "react"
import { ViewStyle, View } from "react-native"
import { LinearGradient } from "expo-linear-gradient"
import { metrics } from "../../theme/metrics"

const BG_GRADIENT: ViewStyle = {
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
}

export interface GradientBackgroundProps {
  colors: string[]
  style: ViewStyle
}

export function GradientBackground(props: GradientBackgroundProps) {
  return (
    <View
      style={{
        position: "absolute",
        left: -metrics.screenWidth / 1.95,
        right: 0,
        top: 0,
        overflow: "hidden",
        width: metrics.screenWidth * 2,
        height: metrics.screenWidth + 100,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: metrics.screenWidth,
        borderBottomRightRadius: metrics.screenWidth,
      }}
    >
      <LinearGradient
        start={{ x: 1, y: 1 }}
        end={{ x: 0, y: 1 }}
        colors={props.colors}
        style={BG_GRADIENT}
      />
    </View>
  )
}
