import React from "react"
import { View, ViewStyle, TextStyle, Text } from "react-native"
import { HeaderProps } from "./header.props"
import { metrics } from "../../theme/metrics"
import { translate } from "../../i18n/"
import { GradientBackground } from ".."
import { observer } from "mobx-react-lite"
import UserStore from "../../services/stores/UserStore"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { useTheme } from "react-native-paper"
import AppStore from "../../services/stores/AppStore"
import { typography } from "../../theme"

// static styles
const ROOT: ViewStyle = {
  flexDirection: "column",
  paddingBottom: metrics.spacing[3],
  paddingRight: metrics.spacing[3],
  paddingLeft: metrics.spacing[3],
}
const headerContent: ViewStyle = { marginLeft: -2 }
const TITLE: TextStyle = { textAlign: "left" }
const TITLE_MIDDLE: ViewStyle = { marginTop: metrics.spacing[6], marginBottom: metrics.spacing[1] }

/**
 * Header that appears on many screens. Will hold navigation buttons and screen title.
 */

export const Header = observer((props: HeaderProps) => {
  const { headerText, headerTx, style, titleStyle, childrenStyle } = props
  const header = headerText || (headerTx && translate(headerTx)) || ""
  const insets = useSafeAreaInsets()
  const paddingTop = insets.bottom == 0 ? 0 : insets.bottom
  const { colors } = useTheme()

  return (
    <View style={[ROOT, style, { paddingTop: paddingTop }]}>
      <GradientBackground
        style={{
          borderRadius: 100,
          overflow: "hidden",
        }}
        colors={[colors.primary, colors.accent]}
      />
      <View style={TITLE_MIDDLE}>
        <Text
          style={{
            color: "#fff",
            fontSize: 14,
            fontFamily: typography.extraBold,
            display: UserStore.accessToken ? "flex" : "none",
          }}
        >
          {translate("welcome")} {UserStore.userInfo.given_name}
        </Text>
      </View>

      <View style={[childrenStyle, headerContent]}>{props.children}</View>
    </View>
  )
})
