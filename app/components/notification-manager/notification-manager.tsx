import { Platform } from "react-native"
import firebase from "@react-native-firebase/app"
import "@react-native-firebase/messaging"
import PushNotification from "react-native-push-notification"
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import { FirebaseMessagingTypes } from "@react-native-firebase/messaging"

interface options {
  largeIcon?: string
  smallIcon?: string
  vibration?: number
  vibrate?: boolean
  alertAction?: string
  category?: string
  playSound?: boolean
  soundName?: string
}

class NotificationManager {
  getToken = () => {
    firebase
      .messaging()
      .getToken(firebase.app().options.messagingSenderId)
      .then((x) => console.log(x))
      .catch((e) => console.log(e))
  }

  registerForRemoteMessages = () => {
    firebase
      .messaging()
      .registerDeviceForRemoteMessages()
      .then(() => {
        console.log("Registered")
      })
      .catch((e) => console.log(e))
  }

  buildAdroidNotification = (title: string, message: string, data = {}, options: options = {}) => {
    return {
      autoCancel: true,
      largeIcon: options.largeIcon || "ic_launcher",
      smallIcon: options.smallIcon || "ic_launcher",
      bigText: message || "",
      subText: title || "",
      vibration: options.vibration || 300,
      vibrate: options.vibrate || false,
      // priority: options.priority || "high",
      // importance: options.importance || "high",
      data: data,
    }
  }
  checkNotification = (data) => {
    PushNotification.getScheduledLocalNotifications((arr) => {
      console.log(arr)
      arr.forEach((noti) => {
        if (noti.id == data.id) {
          console.log("notification zaten var.")
          return false
        }
        return true
      })
    })
  }
  createScheduledNotification = (title: string, body: string, data: { remainingSeconds; id }) => {
    this.checkNotification(data)

    PushNotification.localNotificationSchedule({
      id: data.id,
      title: title,
      message: body! || "",
      vibrate: true,
      date: new Date(Date.now() + data.remainingSeconds),
      playSound: true,
      repeatTime: 1,
      allowWhileIdle: false,
    })
  }

  cancelAllNotification = () => {
    console.log("cancel")
    PushNotification.cancelAllLocalNotifications()
    if (Platform.OS === "ios") {
      PushNotificationIOS.removeAllDeliveredNotifications()
    }
  }

  listenNotification = () => {
    firebase.messaging().onMessage((response) => {
      const notification = response.notification

      if (Platform.OS !== "ios") {
        this.showNotification(notification)
        return
      }
      PushNotificationIOS.requestPermissions().then(() => this.showNotification(notification))
    })
  }
  // showNotification = (notification: FirebaseMessagingTypes.Notification) => {
  //   PushNotification.localNotification({
  //     title: notification.title,
  //     message: notification.body!,
  //   })
  // }

  showNotification = (
    notification: FirebaseMessagingTypes.Notification,
    data = {},
    options: options = {},
    time = new Date(Date.now()),
  ) => {
    console.log(time)
    PushNotification.localNotificationSchedule({
      // id: "123",
      title: notification.title,
      message: notification.body! || "",
      vibrate: true,
      date: new Date(Date.now() + 5 * 1000), // Schedule in 5 secs
      playSound: true,
      repeatTime: 1, // (optional) Increment of configured repeatType. Check 'Repeating Notifications' section for more info.
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    })
  }
  unregister = () => {
    PushNotification.unregister()
  }
}
export const notificationManager = new NotificationManager()
