import TimeTableStore from "../../services/stores/TimeTableStore"
import { notificationManager } from "./notification-manager"
import moment from "moment"

class Notifications {
  constructor() {}
  async upComingLessons() {
    const timetableToday = await TimeTableStore.get(
      moment().format("YYYY-MM-DD"),
      moment().format("YYYY-MM-DD"),
    )
    timetableToday.forEach((lesson) => {
      var duration = moment.duration(
        moment(moment(lesson.Baslangic).format()).diff(moment().format()),
      )
      console.log(lesson)
      if (duration.hours() >= 0) {
        const remainingTimeString = lesson.DersAd + " dersine 1 saatten daha az kaldı"
        notificationManager.createScheduledNotification(
          "Ders saatin yaklaşıyor",
          remainingTimeString,
          {
            id: lesson.DersKod,
            remainingSeconds: duration.asMilliseconds() - 3600000,
          },
        )
      }
    })
  }
}

export const notifications = new Notifications()
