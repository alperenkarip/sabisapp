import React from "react"
import { View, Text } from "react-native"
import Icon from "react-native-vector-icons/EvilIcons"
import { useTheme } from "react-native-paper"

type Props = {
  icon?: Function
  label?: string
  isCurrent?: boolean
}

export const BottomMenuItem = ({ icon, label, isCurrent }: Props) => {
  const props = icon("", 0).props
  const { colors } = useTheme()
  return (
    <View
      style={{
        height: "100%",
        justifyContent: "flex-start",
        alignItems: "center",
        paddingTop: 15,
      }}
    >
      <Icon
        name={props.name}
        size={props.size}
        style={{ color: isCurrent ? colors.primary : "#CFD2D7" }}
      />
      {/* <Text style={{ color: isCurrent ? "#3A36D5" : "#CFD2D7" }}>{label}</Text> */}
    </View>
  )
}
