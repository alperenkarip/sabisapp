import React, { useState, useEffect } from "react"
import { View, TouchableOpacity, Dimensions, Animated, StyleSheet, Text } from "react-native"
import { BottomMenuItem } from "./BottomMenuItem"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { useTheme } from "react-native-paper"

export const TabBar = ({ state, descriptors, navigation }) => {
  const [translateValue] = useState(new Animated.Value(0))
  const totalWidth = Dimensions.get("window").width
  const tabWidth = totalWidth / state.routes.length

  const animateSlider = (index: number) => {
    Animated.spring(translateValue, {
      toValue: index * tabWidth,
      velocity: 10,
      useNativeDriver: true,
    }).start()
  }

  useEffect(() => {
    animateSlider(state.index)
  }, [state.index])
  const insets = useSafeAreaInsets()
  const bottomHeight = insets.bottom == 0 ? 50 : 75
  const { colors } = useTheme()

  return (
    <View
      style={[
        style.tabContainer,
        { height: bottomHeight, paddingBottom: insets.bottom, width: totalWidth },
      ]}
    >
      <View style={{ flexDirection: "row" }}>
        <Animated.View
          style={[
            style.slider,
            {
              backgroundColor: colors.primary,
              transform: [{ translateX: translateValue }],
              width: tabWidth - 20,
            },
          ]}
        />

        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key]
          const icon = options.tabBarIcon !== undefined ? options.tabBarIcon : undefined
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name

          const isFocused = state.index === index

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            })

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name)
            }

            animateSlider(index)
          }

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            })
          }

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityStates={isFocused ? ["selected"] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{ flex: 1 }}
              key={index}
            >
              <BottomMenuItem icon={icon} isCurrent={isFocused} label={label.toString()} />
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  tabContainer: {
    height: 50,
    position: "relative",
    // bottom: 0,
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.0,
    backgroundColor: "white",
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    elevation: 10,
  },
  slider: {
    height: 5,
    position: "absolute",
    top: 0,
    left: 10,
    borderRadius: 10,
  },
})
