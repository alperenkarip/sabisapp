/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import React from "react"
import { useColorScheme } from "react-native"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {
  CardScreen,
  ThemeSettings,
  FoodMenuScreen,
  UserHomeScreen,
  LessonDetailScreen,
  LessonsScreen,
  LoginScreen,
  SearchPersonScreen,
  ShortcutsScreen,
  TimetableScreen,
  UserProfileScreen,
  WelcomeScreen,
  AppLanguageScreen,
} from "../screens"
import { SearchScreen } from "../screens"
import { navigationRef } from "./navigation-utilities"
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"
import Icon from "react-native-vector-icons/EvilIcons"
import { TabBar } from "./TabBar"

import { createSharedElementStackNavigator } from "react-navigation-shared-element"
import { enableScreens } from "react-native-screens"

enableScreens()

export type NavigatorParamList = {
  Welcome: undefined
  Search: undefined
  ThemeSettings
  Calendar
  AppLanguage
  Shortcuts
  Login
  TimeTable
  LessonScreen
  sliderData: {
    Baslik
    Link
  }
  LessonDetail: {
    index: 0
    item: {
      DersBasariPaket: {
        AnketDoldurmusMu: false
        BasariNot: {
          BagilOrtalama: 0
          BasariNot: 0
          EnumBasariNot: 0
          MutlakOrtalama: 0
          NotID: 0
          TarihYayinlanma: ""
        }
        GrupID: 0
        PayList: []
      }
      DersBilgisi: {
        AKTS: 0
        DSaat: 0
        DersAd: ""
        DersAdYabanci: ""
        DersKod: ""
        DersPlanAnaID: 0
        EnumDersBirimTipi: 0
        EnumDersSecimTipi: 0
        EnumKokDersTipi: 0
        USaat: 0
        UzemMi: false
      }
      GrupBilgisi: {
        Birim: ""
        DersProgramiList: ""
        EnumOgretimTur: 0
        Fakulte: ""
        GrupAd: ""
        GrupID: 0
        OgretimGorevlisiList: ""
      }
      YazilmaBilgisi: {
        DevamZorunluluguDisinda: false
        EnumIptalSebebi: ""
        Iptal: false
        YazilmaID: 0
        YazilmaTarihi: ""
      }
    }
  }
  FoodMenu
  Card
  SearchPerson
  isAuth
}

// const Tab = createBottomTabNavigator()
const Tab = createMaterialTopTabNavigator()

const WelcomeStack = createStackNavigator()
const Stack = createStackNavigator()
const LessonsStack = createSharedElementStackNavigator()
const UserProfileStack = createStackNavigator()

function WelcomeStackScreen() {
  return (
    <WelcomeStack.Navigator
      screenOptions={{
        headerShown: false,
        transitionSpec: {
          open: { animation: "timing", config: { duration: 300 } },
          close: { animation: "timing", config: { duration: 300 } },
        },
        cardStyleInterpolator: ({ current: { progress } }) => {
          return {
            cardStyle: {
              opacity: progress,
            },
          }
        },
      }}
    >
      <WelcomeStack.Screen name="Welcome" component={WelcomeScreen} />
      <WelcomeStack.Screen name="Search" component={SearchScreen} />
      <WelcomeStack.Screen name="FoodMenu" component={FoodMenuScreen} />
      <WelcomeStack.Screen name="Card" component={CardScreen} />
      <WelcomeStack.Screen name="SearchPerson" component={SearchPersonScreen} />
    </WelcomeStack.Navigator>
  )
}

function LessonsStackScreen() {
  return (
    <LessonsStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <LessonsStack.Screen name="Lessons" component={LessonsScreen} />
      <LessonsStack.Screen
        name="LessonsDetail"
        component={LessonDetailScreen}
        options={() => ({
          // gestureEnabled: false,
          transitionSpec: {
            open: { animation: "timing", config: { duration: 400 } },
            close: { animation: "timing", config: { duration: 400 } },
          },
          cardStyleInterpolator: ({ current: { progress } }) => {
            return {
              cardStyle: {
                opacity: progress,
              },
            }
          },
        })}
        sharedElements={(route, otherRoute, showing) => {
          const item = route.params.index
          return [
            {
              id: "lessonID" + item,
              animation: "move",
            },
            {
              id: "deneme",
              animation: "fade",
            },
          ]
        }}
      />
    </LessonsStack.Navigator>
  )
}

function UserProfileStackScreen() {
  return (
    <UserProfileStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <UserProfileStack.Screen name="UserProfile" component={UserProfileScreen} />
      <UserProfileStack.Screen name="ThemeSettings" component={ThemeSettings} />
      <UserProfileStack.Screen name="Shortcuts" component={ShortcutsScreen} />
      <UserProfileStack.Screen name="AppLanguage" component={AppLanguageScreen} />
    </UserProfileStack.Navigator>
  )
}

function Tabs() {
  return (
    <Tab.Navigator tabBarPosition="bottom" tabBar={(props) => <TabBar {...props} />}>
      <Tab.Screen
        name="WelcomeStack"
        component={WelcomeStackScreen}
        options={{
          swipeEnabled: false,
          tabBarIcon: ({ color }) => <Icon name="archive" size={35} />,
        }}
      />
      <Tab.Screen
        name="UserHome"
        component={UserHomeScreen}
        options={{
          swipeEnabled: false,

          tabBarIcon: ({ color }) => <Icon name="paperclip" size={35} />,
        }}
      />
      <Tab.Screen
        name="LessonsStack"
        component={LessonsStackScreen}
        options={{
          swipeEnabled: false,

          tabBarIcon: ({ color }) => <Icon name="navicon" size={35} />,
        }}
      />
      <Tab.Screen
        name="TimeTable"
        component={TimetableScreen}
        options={{
          swipeEnabled: false,

          tabBarIcon: ({ color }) => <Icon name="calendar" size={35} />,
        }}
      />
      <Tab.Screen
        name="UserProfileStack"
        component={UserProfileStackScreen}
        options={{
          swipeEnabled: false,

          tabBarIcon: ({ color }) => <Icon name="user" size={35} />,
        }}
      />
    </Tab.Navigator>
  )
}

interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {
  isAuth
}

export const AppNavigator = (props: NavigationProps) => {
  return (
    <NavigationContainer ref={navigationRef} {...props}>
      {/* <MyTabs /> */}
      {/* initialRouteName={isAuth() ? Home : Login} */}

      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        {props.isAuth ? (
          <Stack.Screen name="Landing" component={Tabs} />
        ) : (
          <Stack.Screen name="Login" component={LoginScreen} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

AppNavigator.displayName = "AppNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["login"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
