import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"
import { metrics } from "../../theme/metrics"
import { theme } from "../../theme/default"
import Icon from "react-native-vector-icons/EvilIcons"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { useEffect } from "react"
import FoodCardStore from "../../services/stores/FoodCardStore"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

export const CardScreen: FC<StackScreenProps<NavigatorParamList, "Card">> = observer(
  function CardScreen({ navigation }) {
    const { colors } = useTheme()

    useEffect(() => {
      FoodCardStore.get()
    })
    const renderItem = ({ item }) => (
      <View style={{ ...MENU_ITEM }}>
        <TouchableOpacity onPress={() => {}}>
          <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>{item.title}</Text>
          <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
            <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: colors.text,
                  paddingLeft: metrics.spacing[1],
                }}
              >
                {translate("balance")}
              </Text>
            </View>
            <View style={{ ...MENU_ITEM_SUB, flex: 1 }}>
              <Text
                style={{ ...MENU_ITEM_TITLE, color: colors.text, paddingLeft: metrics.spacing[1] }}
              >
                {translate("previousBalance")}
              </Text>
            </View>
            <View style={{ ...MENU_ITEM_SUB, flex: 1 }}>
              <Text
                style={{ ...MENU_ITEM_TITLE, color: colors.text, paddingLeft: metrics.spacing[1] }}
              >
                {translate("date")}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
            <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
              <Icon name="cart" size={25} color="#4F8EF7" />
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: colors.text,
                  paddingLeft: metrics.spacing[1],
                }}
              >
                {item.balance}
              </Text>
            </View>
            <View style={{ ...MENU_ITEM_SUB, flex: 1 }}>
              <Icon name="cart" size={25} color="#4F8EF7" />
              <Text
                style={{ ...MENU_ITEM_TITLE, color: colors.text, paddingLeft: metrics.spacing[1] }}
              >
                {item.previousBalance}
              </Text>
            </View>
            <View style={{ ...MENU_ITEM_SUB, flex: 1 }}>
              <Icon name="calendar" size={25} color="#4F8EF7" />
              <Text
                style={{ ...MENU_ITEM_TITLE, color: colors.text, paddingLeft: metrics.spacing[1] }}
              >
                {item.date}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
    return (
      <View style={{ flex: 1 }}>
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              style={{ zIndex: 3, elevation: 3, width: 20 }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={40}
                color="#fff"
                style={{ marginLeft: -metrics.spacing[3] }}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerText }}>{translate("cardInfo")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <Text
            style={{
              textAlign: "center",
              fontFamily: typography.semiBold,
              color: "#fff",
              marginBottom: metrics.spacing[1],
              fontSize: 16,
            }}
          >
            {translate("cardBalance")} : {FoodCardStore.balance}
          </Text>
          <FlatList
            data={FoodCardStore.list}
            renderItem={renderItem}
            style={{ marginBottom: 50 }}
            keyExtractor={(item) => item.id}
          />
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
}

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}
