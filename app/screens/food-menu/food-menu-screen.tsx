import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Button, Header, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"
import { theme } from "../../theme/default"
import DateTimePicker from "react-native-modal-datetime-picker"
import Icon from "react-native-vector-icons/EvilIcons"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { metrics } from "../../theme/metrics"
import { useEffect } from "react"
import FoodMenuStore from "../../services/stores/FoodMenuStore"
import moment from "moment"
import "moment/min/locales"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

const ROOT: ViewStyle = {
  flex: 1,
  paddingHorizontal: metrics.spacing[2],
}
const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}

export const FoodMenuScreen: FC<StackScreenProps<NavigatorParamList, "FoodMenu">> = observer(
  function FoodMenuScreen({ navigation }) {
    const { colors } = useTheme()

    useEffect(() => {
      const date = moment().toDate()
      const dateObj = {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear(),
      }
      console.log(dateObj)
      // if (!FoodMenuStore.dateObj.day) FoodMenuStore.dateSet(dateObj)

      // hata var, component rerender oluyor
      handleDatePicked(moment(dateObj.year + "/" + dateObj.month + "/" + dateObj.day).toDate())
      // FoodMenuStore.get()
    }, [])

    const [pickedDateString, setPickedDateString] = React.useState("")

    const showDateTimePicker = () => {
      setisDateTimePickerVisible(true)
    }

    const hideDateTimePicker = () => {
      setisDateTimePickerVisible(false)
    }

    const handleDatePicked = (pickedDate) => {
      const dateParams = moment(pickedDate)
      const dateObj = {
        day: parseInt(moment(dateParams).format("D")),
        month: parseInt(moment(dateParams).format("M")),
        year: parseInt(moment(dateParams).format("Y")),
      }
      setPickedDateString(dateParams.format("DD MMM YYYY ddd"))
      setisDateTimePickerVisible(false)
      FoodMenuStore.dateSet(dateObj)
      FoodMenuStore.dietSet(false)
      FoodMenuStore.get()
    }
    const [isDateTimePickerVisible, setisDateTimePickerVisible] = useState(false)
    const renderItem = ({ item }) => (
      <View style={MENU_ITEM}>
        <TouchableOpacity onPress={() => {}}>
          <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>{item.YemekAd}</Text>
          <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
            <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 2 }}>
              <Icon name="archive" size={25} color={colors.primary} />
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: color.line,
                  paddingLeft: metrics.spacing[1],
                }}
              >
                {item.Malzeme}
              </Text>
            </View>
            <View style={{ ...MENU_ITEM_SUB, flex: 1 }}>
              <Text
                style={{ ...MENU_ITEM_TITLE, color: color.line, paddingLeft: metrics.spacing[1] }}
              >
                {item.Kalori} {translate("calorie")}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
    return (
      <View style={{ flex: 1 }}>
        <DateTimePicker
          isVisible={isDateTimePickerVisible}
          onConfirm={handleDatePicked}
          onCancel={hideDateTimePicker}
          locale="tr"
          titleIOS="Tarih seçiniz"
          cancelTextIOS="İptal"
          confirmTextIOS="Tamam"
          date={moment(
            FoodMenuStore.dateObj.year +
              "/" +
              FoodMenuStore.dateObj.month +
              "/" +
              FoodMenuStore.dateObj.day,
          ).toDate()}
        />
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              style={{ zIndex: 3, elevation: 3, width: 20 }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={40}
                color="#fff"
                style={{ marginLeft: -metrics.spacing[3] }}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerTextWithSub }}>{translate("foodMenu")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              marginTop: -10,
            }}
          >
            <Icon name="clock" size={25} color="#fff" />
            <Button
              onPress={showDateTimePicker}
              textStyle={{ fontSize: 17, paddingLeft: 5 }}
              style={{
                paddingLeft: 0,
                marginLeft: 0,
                paddingRight: 0,
                marginBottom: metrics.spacing[2],
                backgroundColor: "transparent",
                borderBottomWidth: 1,
                borderColor: color.white,
              }}
              text={pickedDateString ? pickedDateString : "Tarih Seçiniz"}
            />
            <Icon name="undo" size={25} color="#fff" />
            <Button
              textStyle={{ fontSize: 17, paddingLeft: 3 }}
              style={{
                paddingLeft: 0,
                paddingRight: 0,
                borderBottomWidth: 1,
                borderColor: color.white,
                marginBottom: metrics.spacing[2],
                backgroundColor: "transparent",
              }}
              onPress={() => {
                FoodMenuStore.dietSet(!FoodMenuStore.diet)
                FoodMenuStore.get()
              }}
              text={FoodMenuStore.diet ? "Diyet Menü" : "Normal Menü"}
            />
          </View>
          <Text
            style={{
              textAlign: "center",
              fontFamily: typography.semiBold,
              color: "#fff",
              marginBottom: metrics.spacing[1],
            }}
          >
            {FoodMenuStore.diet ? translate("dietMenuText") : translate("normalMenuText")}
          </Text>
          {FoodMenuStore.foodList === null ? (
            <View style={MENU_ITEM}>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: metrics.spacing[1],
                  justifyContent: "center",
                }}
              >
                <Text style={{ margin: metrics.spacing[2], fontSize: 20, textAlign: "center" }}>
                  {translate("noFood")}
                </Text>
              </View>
            </View>
          ) : (
            <FlatList
              data={FoodMenuStore.foodList.toJSON()}
              renderItem={renderItem}
              style={{ marginBottom: 50 }}
              keyExtractor={(item, index) => "key" + index}
            />
          )}
        </Screen>
      </View>
    )
  },
)
