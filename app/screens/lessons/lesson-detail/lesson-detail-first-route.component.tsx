import * as React from "react"
import { FlatList, Text, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../../theme"
import { metrics } from "../../../theme/metrics"
import { theme } from "../../../theme/default"
import Icon2 from "react-native-vector-icons/FontAwesome"
import Icon3 from "react-native-vector-icons/SimpleLineIcons"
import Icon4 from "react-native-vector-icons/AntDesign"
import { palette } from "../../../theme/palette"
import moment from "moment"
import { useTheme } from "react-native-paper"
import { translate } from "../../../i18n"

export const LessonDetailFirstRoute = observer(function LessonDetailFirstRoute(elem) {
  const {} = elem
  const { colors } = useTheme()

  return (
    <View style={{ flex: 1, marginTop: metrics.spacing[2] }}>
      <FlatList
        data={elem.props.DersBasariPaket.PayList}
        renderItem={(data) => {
          return (
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={LEFT_INDICATOR_CIRCLE}>
                <Icon2 name="circle-o" size={16} color={colors.primary} />
                <View style={LEFT_INDICATOR_LINE}></View>
              </View>
              <View style={LIST}>
                {/* <Text>{JSON.stringify(item.DersNot)}</Text> */}
                <Text style={{ ...LIST_DATE, color: colors.text }}>
                  {data.item.DersNot
                    ? moment(data.item.DersNot.AciklanmaTarihi).format("YYYY-MM-DD HH:mm")
                    : ""}
                </Text>
                <View style={MENU_ITEM}>
                  <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                    {data.item.CalismaTipAd}
                  </Text>
                  <View style={LIST_VALUES}>
                    <View style={MENU_ITEM_SUB}>
                      <Text style={{ ...LIST_VALUE }}>{translate("totalContribution")}</Text>
                    </View>
                    <View style={MENU_ITEM_SUB}>
                      <Text style={LIST_VALUE}>{translate("note")}</Text>
                    </View>
                    <View style={MENU_ITEM_SUB}>
                      <Text style={LIST_VALUE}>{translate("finalPercentage")}</Text>
                    </View>
                  </View>
                  <View style={LIST_VALUES}>
                    <View style={MENU_ITEM_SUB}>
                      <Icon4 name="swap" size={20} color={colors.primary} />
                      <Text style={LIST_VALUE}>
                        {data.item.DersNot ? data.item.DersNot.ToplamKatki : ""}
                      </Text>
                    </View>
                    <View style={MENU_ITEM_SUB}>
                      <Icon3 name="note" size={16} color={colors.primary} />
                      <Text style={LIST_VALUE}>
                        {data.item.DersNot ? data.item.DersNot.Not : ""}
                      </Text>
                    </View>
                    <View style={MENU_ITEM_SUB}>
                      <Icon2 name="calculator" size={16} color={colors.primary} />
                      <Text style={LIST_VALUE}>{data.item.NihaiYuzde}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )
        }}
        style={{ marginBottom: 0, flex: 1 }}
        keyExtractor={(item, index) => "key" + index}
      />
    </View>
  )
})

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}
const LEFT_INDICATOR_CIRCLE: ViewStyle = {
  width: 15,
  paddingTop: metrics.spacing[1],
  paddingBottom: 25,
}
const LEFT_INDICATOR_LINE: ViewStyle = {
  width: 1,
  backgroundColor: palette.grey400,
  position: "absolute",
  height: "100%",
  marginTop: 25,
  left: 6.5,
}
const LIST: ViewStyle = {
  flex: 1,
  paddingLeft: metrics.spacing[2],
  marginBottom: metrics.spacing[5],
}
const LIST_DATE: TextStyle = {
  fontFamily: typography.semiBold,
  marginBottom: metrics.spacing[2],
}
const LIST_VALUES: ViewStyle = { flexDirection: "row", marginTop: metrics.spacing[1] }
const LIST_VALUE: TextStyle = {
  ...MENU_ITEM_TITLE,
  paddingLeft: 2,
  color: color.line,
}
