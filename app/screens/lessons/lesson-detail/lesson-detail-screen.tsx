import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing, typography } from "../../../theme"
import { theme } from "../../../theme/default"
import Icon from "react-native-vector-icons/EvilIcons"
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons"
import { metrics } from "../../../theme/metrics"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../../navigators"
import { SceneMap, TabBar, TabView } from "react-native-tab-view"
import { LessonDetailFirstRoute } from "./lesson-detail-first-route.component"
import { LessonDetailSecondRoute } from "./lesson-detail-second-route.component"
import { SharedElement } from "react-navigation-shared-element"
import { LinearGradient } from "expo-linear-gradient"
import { useEffect } from "react"
import LessonsStore from "../../../services/stores/LessonsStore"
import { useTheme } from "react-native-paper"
import { translate } from "../../../i18n"
import AppStore from "../../../services/stores/AppStore"

export const LessonDetailScreen: FC<
  StackScreenProps<NavigatorParamList, "LessonDetail">
> = observer(({ navigation, route }) => {
  const { colors } = useTheme()

  useEffect(() => {
    LessonsStore.getAnnouncements(route.params.item.GrupBilgisi.GrupID)
  }, [])

  const [index, setIndex] = React.useState(0)

  const [tabTitles, settabTitles] = useState([translate("notes"), translate("announcements")])

  const changeTabTitles = () => {
    settabTitles([translate("notes"), translate("announcements")])
  }

  useEffect(() => {
    console.log(123)
    // changeTabTitles()
  })

  const renderScene = SceneMap({
    first: () => {
      return <LessonDetailFirstRoute props={route.params.item} />
    },
    second: () => {
      return <LessonDetailSecondRoute props={LessonsStore.lessonAnnouncements} />
    },
  })
  function Tab() {
    const [routes] = React.useState([
      { title: translate("notes"), key: "first" },
      { title: translate("announcements"), key: "second" },
    ])
    return (
      <TabView
        renderTabBar={(props) => (
          <TabBar
            {...props}
            renderLabel={({ route, focused, color }) => (
              <Text style={{ color, margin: 8, fontSize: 14 }}>{route.title}</Text>
            )}
            tabStyle={{
              paddingTop: 0,
            }}
            contentContainerStyle={{
              justifyContent: "center",
            }}
            indicatorStyle={{
              padding: 5,
              height: "100%",
              borderRadius: 30,
              backgroundColor: color.white,
              borderColor: color.dim,
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 0 },
              shadowOpacity: 0.1,
              shadowRadius: 3,
            }}
            labelStyle={{
              color: colors.primary,
              fontFamily: typography.bold,
            }}
            inactiveColor="#bfbfbf"
            style={{
              borderRadius: 30,
              height: 38,
              backgroundColor: "#f6f6f6",
            }}
          />
        )}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
      />
    )
  }
  return (
    <View style={{ flex: 1 }}>
      <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity
            style={{ zIndex: 3, elevation: 3, width: 20 }}
            onPress={() => navigation.goBack()}
          >
            <Icon
              name="chevron-left"
              size={40}
              color="#fff"
              style={{ marginLeft: -metrics.spacing[3] }}
            />
          </TouchableOpacity>
          <View style={{ flex: 1, marginLeft: -20 }}>
            <Text style={{ ...theme.headerTextWithSub }}>
              {route.params.item.DersBilgisi.DersAd}
            </Text>
          </View>
        </View>
        <SharedElement id={"lessonID" + route.params.index}>
          <View
            style={{
              ...MENU_ITEM,
              marginBottom: 0,
              borderBottomLeftRadius: 0,
              borderBottomRightRadius: 0,
            }}
          >
            <TouchableOpacity>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {route.params.item.DersBilgisi.DersAd}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={MENU_ITEM_SUB}>
                  <Icon name="clock" size={25} color={colors.primary} />
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      marginLeft: metrics.spacing[1],
                      color: color.line,
                    }}
                  >
                    {route.params.item.DersBilgisi.DSaat} {translate("hour")}
                  </Text>
                </View>

                <View style={MENU_ITEM_SUB}>
                  <IconMaterial name="archive" size={25} color={colors.primary} />
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      marginLeft: metrics.spacing[1],
                    }}
                  >
                    {route.params.item.GrupBilgisi.GrupAd} {translate("group")}
                  </Text>
                </View>
              </View>

              <View style={CORNER_CONTAINER}>
                <View style={CORNER_CIRCLE}>
                  <LinearGradient
                    colors={[colors.primary, colors.accent]}
                    start={{ x: 1.3, y: 1 }}
                    end={{ x: 0, y: 1 }}
                    style={CORNER_GRADIENT}
                  />
                  <Text style={CORNER_CIRCLE_TEXT}>AA</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </SharedElement>
        <View
          style={{
            ...MENU_ITEM,
            marginTop: -metrics.spacing[1],
            // borderWidth: 1,
            // borderColor: "red",
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            paddingTop: 0,
          }}
        >
          <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
            <View style={MENU_ITEM_SUB}>
              <IconMaterial name="file-document" size={25} color={colors.primary} />
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: color.line,

                  marginLeft: metrics.spacing[1],
                }}
              >
                {route.params.item.DersBilgisi.AKTS} AKTS
              </Text>
            </View>

            <View style={MENU_ITEM_SUB}>
              <IconMaterial name="barcode" size={25} color={colors.primary} />
              <Text
                style={{ ...MENU_ITEM_TITLE, color: color.line, marginLeft: metrics.spacing[1] }}
              >
                {route.params.item.DersBilgisi.DersKod}
              </Text>
            </View>
          </View>
        </View>
      </Header>
      <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
        <Tab />
      </Screen>
    </View>
  )
})

const ROOT: ViewStyle = {
  flex: 1,
  paddingHorizontal: metrics.spacing[4],
}

const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
  color: color.white,
}

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}
const CORNER_CONTAINER: ViewStyle = {
  position: "absolute",
  right: -20,
  top: -30,
}
const CORNER_CIRCLE: ViewStyle = {
  width: 65,
  height: 65,
  justifyContent: "center",
  alignItems: "center",
  paddingTop: 17,
  paddingRight: 5,
}
const CORNER_GRADIENT: ViewStyle = {
  borderRadius: 33,
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
}
const CORNER_CIRCLE_TEXT: TextStyle = {
  fontFamily: typography.extraBold,
  fontSize: 17,
  color: color.white,
}
