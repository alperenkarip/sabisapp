import * as React from "react"
import {
  StyleProp,
  TextStyle,
  View,
  ViewStyle,
  Text,
  FlatList,
  TouchableOpacity,
} from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../../theme"
import { metrics } from "../../../theme/metrics"
import { theme } from "../../../theme/default"
import LessonsStore from "../../../services/stores/LessonsStore"
import { useTheme } from "react-native-paper"

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
export const LessonDetailSecondRoute = observer(function (props) {
  const { colors } = useTheme()

  const renderItem = ({ item }) => (
    <View style={MENU_ITEM}>
      <TouchableOpacity>
        <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>{item.Konu}</Text>
        <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
          <Text
            style={{
              ...MENU_ITEM_TITLE,
              color: color.line,
            }}
          >
            {item.Mesaj.replace(/<\/?[^>]+(>|$)/g, "")
              .replace(/&nbsp;/g, " ")
              .replace(/&quot;/g, " ")
              .replace(/&rsquo;/g, " ")
              .replace(/&#39;/g, " ")
              .replace(/&ccedil;/g, "ç")
              .replace(/&#287;/g, "ğ")
              .replace(/&#305;/g, "ı")
              .replace(/&ouml;/g, "ö")
              .replace(/&#351;/g, "ş")
              .replace(/&uuml;/g, "ü")
              .replace(/&Ccedil;/g, "Ç")
              .replace(/&#286;/g, "Ğ")
              .replace(/&#304;/g, "I")
              .replace(/&Ouml;/g, "Ö")
              .replace(/&#350;/g, "Ş")
              .replace(/&Uuml;/g, "Ü")}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
  return (
    <FlatList
      data={LessonsStore.lessonAnnouncements}
      renderItem={renderItem}
      style={{ marginBottom: 0, flex: 1 }}
      keyExtractor={(item, index) => "key" + index}
    />
  )
})
