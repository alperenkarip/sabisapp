import React, { FC, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../components"
import { color, typography } from "../../theme"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import { LinearGradient } from "expo-linear-gradient"
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons"
import Icon from "react-native-vector-icons/EvilIcons"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { SharedElement } from "react-navigation-shared-element"
import LessonsStore from "../../services/stores/LessonsStore"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

export const LessonsScreen: FC<StackScreenProps<NavigatorParamList, "welcome">> = observer(
  function LessonsScreen({ navigation }) {
    const { colors } = useTheme()

    useEffect(() => {
      LessonsStore.getLessons()
    }, [])
    const renderItem = ({ index, item }) => (
      <SharedElement id={"lessonID" + index}>
        <View style={MENU_ITEM}>
          <TouchableOpacity
            onPress={() => navigation.navigate("LessonsDetail", { index: index, item: item })}
          >
            <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
              {item.DersBilgisi.DersAd}
            </Text>
            <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
              <View style={MENU_ITEM_SUB}>
                <Icon name="clock" size={25} color={colors.primary} />
                <Text
                  style={{
                    ...MENU_ITEM_TITLE,
                    color: color.line,
                    marginLeft: metrics.spacing[1],
                  }}
                >
                  {item.DersBilgisi.DSaat} {translate("hour")}
                </Text>
              </View>

              <View style={MENU_ITEM_SUB}>
                <IconMaterial name="archive" size={25} color={colors.primary} />
                <Text
                  style={{ ...MENU_ITEM_TITLE, color: color.line, marginLeft: metrics.spacing[1] }}
                >
                  {item.GrupBilgisi.GrupAd} {translate("group")}
                </Text>
              </View>
            </View>
            <View style={CORNER_CONTAINER}>
              <View style={CORNER_CIRCLE}>
                <LinearGradient
                  colors={[colors.primary, colors.accent]}
                  start={{ x: 1.3, y: 1 }}
                  end={{ x: 0, y: 1 }}
                  style={CORNER_GRADIENT}
                />
                <Text style={CORNER_CIRCLE_TEXT}>AA</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </SharedElement>
    )
    return (
      <View style={{ flex: 1 }}>
        <Header titleStyle={theme.headerWelcome}>
          <Text style={theme.headerText}>{translate("lessons")}</Text>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <FlatList
            data={LessonsStore.lessons.toJSON()}
            renderItem={renderItem}
            style={{ marginBottom: 50 }}
            keyExtractor={(item, index) => "key" + index}
          />
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  flex: 1,
  paddingHorizontal: metrics.spacing[2],
}

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}
const CORNER_CONTAINER: ViewStyle = {
  position: "absolute",
  right: -20,
  top: -30,
}
const CORNER_CIRCLE: ViewStyle = {
  width: 65,
  height: 65,
  justifyContent: "center",
  alignItems: "center",
  paddingTop: 17,
  paddingRight: 5,
}
const CORNER_GRADIENT: ViewStyle = {
  borderRadius: 33,
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
}
const CORNER_CIRCLE_TEXT: TextStyle = {
  fontFamily: typography.extraBold,
  fontSize: 17,
  color: color.white,
}
