import React, { FC } from "react"
import { View, ViewStyle, TextStyle } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Button, Header, Screen, Text } from "../../components"
import { color, spacing, typography } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import UserStore from "../../services/stores/UserStore"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}

const HEADER_TEXT: TextStyle = {
  ...theme.headerText,
  textAlign: "center",
}
const HEADER_CONTENT: ViewStyle = {
  // marginTop: metrics.spacing[5],
  marginBottom: metrics.spacing[3],
}

export const LoginScreen: FC<StackScreenProps<NavigatorParamList, "Login">> = observer(
  ({ navigation }) => {
    const { colors } = useTheme()
    return (
      <View testID="LoginScreen" style={FULL}>
        <Header titleStyle={theme.headerWelcome}>
          <View style={HEADER_CONTENT}>
            <Text style={{ ...HEADER_TEXT, marginTop: metrics.spacing[5] }}>Hoş Geldin</Text>
            <Text
              style={{
                ...HEADER_TEXT,
                fontSize: 16,
                fontFamily: typography.primary,
                marginTop: metrics.spacing[1],
              }}
            >
              {translate("loginInfo")}
            </Text>
          </View>
        </Header>
        <Screen unsafe={true} style={CONTAINER} backgroundColor={color.transparent}>
          <View style={{ flexDirection: "column" }}>
            <View style={{ marginVertical: metrics.spacing[2] }}>
              <Button
                style={{ borderRadius: 10, backgroundColor: colors.primary }}
                onPress={() => {
                  UserStore.Authorize(() => {
                    UserStore.GetUserInfo()
                  })
                }}
              >
                <Text style={{ color: color.white }}>{translate("login")}</Text>
              </Button>
            </View>
          </View>
        </Screen>
      </View>
    )
  },
)
