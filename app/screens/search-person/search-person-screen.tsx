import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Button, Header, Screen, Text, TextField } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"
import { metrics } from "../../theme/metrics"
import { theme } from "../../theme/default"
import Icon from "react-native-vector-icons/EvilIcons"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import PersonnelSearchStore from "../../services/stores/PersonnelSearchStore"
import DropDownPicker from "react-native-dropdown-picker"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

export const SearchPersonScreen: FC<
  StackScreenProps<NavigatorParamList, "SearchPerson">
> = observer(function SearchPersonScreen({ navigation }) {
  const { colors } = useTheme()

  useEffect(() => {
    PersonnelSearchStore.getUnits()
  }, [])
  const renderItem = ({ item }) => (
    <View style={MENU_ITEM}>
      <TouchableOpacity onPress={() => {}}>
        <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
          {item.Unvan} {item.Ad} {item.Soyad}
        </Text>
        <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
          <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
            <Text
              style={{
                ...MENU_ITEM_TITLE,
                color: color.line,
              }}
            >
              {item.BirimAd}
            </Text>
            <Text
              style={{
                ...MENU_ITEM_TITLE,
                color: color.line,
              }}
            >
              {translate("extensionNumber")} : {item.DahiliNo}
            </Text>
            {item.EPosta ? (
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: color.line,
                }}
              >
                {item.EPosta}
              </Text>
            ) : null}
            {item.Aciklama ? (
              <Text
                style={{
                  ...MENU_ITEM_TITLE,
                  color: color.line,
                }}
              >
                {translate("explanation")} : {item.Aciklama}
              </Text>
            ) : null}
          </View>
        </View>
      </TouchableOpacity>
    </View>
  )

  const [open, setOpen] = useState(false)
  const [unitID, setUnitID] = useState(null)
  const [items, setItems] = useState([])
  const [personnelName, setPersonnelName] = useState("")

  return (
    <View style={{ flex: 1 }}>
      <Header style={{ paddingBottom: metrics.spacing[0] }} titleStyle={theme.headerWelcome}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity
            style={{ zIndex: 3, elevation: 3, width: 20 }}
            onPress={() => navigation.goBack()}
          >
            <Icon
              name="chevron-left"
              size={40}
              color="#fff"
              style={{ marginLeft: -metrics.spacing[3] }}
            />
          </TouchableOpacity>
          <View style={{ flex: 1, marginLeft: -20 }}>
            <Text style={{ ...theme.headerText }}>Personel Arama</Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ flex: 1, marginVertical: metrics.spacing[3] }}>
            <TextField
              placeholder={translate("whatAreYouLookingFor")}
              inputStyle={SEARCH_INPUT_STYLE}
              style={SEARCH_INPUT_CONTAINER}
              value={personnelName}
              onChangeText={(value) => {
                setPersonnelName(value)
              }}
            />
          </View>
        </View>
      </Header>
      <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
        <View style={HEADER_CONTENT}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ flex: 1, height: 60 }}>
              <DropDownPicker
                open={open}
                value={unitID}
                items={PersonnelSearchStore.units}
                setOpen={setOpen}
                setValue={setUnitID}
                setItems={setItems}
                zIndex={9999}
                style={{
                  borderRadius: 20,
                  borderWidth: 0,
                  height: 44,
                }}
                dropDownContainerStyle={{
                  borderWidth: 0,
                }}
                textStyle={{ color: color.text }}
                selectedItemLabelStyle={{
                  color: colors.primary,
                }}
                customItemContainerStyle={{
                  borderWidth: 0,
                  borderColor: "red",
                }}
                customItemLabelStyle={{
                  fontFamily: typography.primary,
                }}
                placeholder="Birim seçin"
                placeholderStyle={{
                  fontSize: 16,
                  minHeight: 10,
                  backgroundColor: color.palette.white,
                  borderColor: colors.primary,
                  color: color.palette.blueGrey300,
                  fontFamily: typography.primary,
                }}
              />
            </View>
          </View>
        </View>
        <Button
          text="Ara"
          preset="second"
          textStyle={{ fontSize: 16 }}
          onPress={() => {
            PersonnelSearchStore.search(personnelName, unitID)
          }}
        />
        <View
          style={{
            position: "relative",
            zIndex: 0,
            marginTop: metrics.spacing[2],
            flex: 1,
          }}
        >
          {PersonnelSearchStore.personnels.length != 0 ? (
            <FlatList
              data={PersonnelSearchStore.personnels}
              renderItem={renderItem}
              style={{ marginBottom: 20 }}
              keyExtractor={(item, index) => "key" + index}
            />
          ) : (
            <View style={MENU_ITEM}>
              <Text style={{ textAlign: "center", fontSize: 17 }}>{translate("noPerson")}</Text>
            </View>
          )}
        </View>
      </Screen>
    </View>
  )
})

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[3],
  marginBottom: metrics.spacing[6],
  flex: 1,
}

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 13.5,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "column",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "flex-start",
  flex: 1,
}
const HEADER_CONTENT: ViewStyle = {
  marginTop: metrics.spacing[0],
  position: "relative",
  zIndex: 2,
}

const SEARCH_INPUT_CONTAINER: ViewStyle = {}
const SEARCH_INPUT_STYLE: TextStyle = {
  borderRadius: 20,
  fontSize: 16,
  paddingHorizontal: metrics.spacing[3],
}
const HEADER_TEXT: TextStyle = {
  ...theme.headerText,
  textAlign: "center",
  marginBottom: metrics.spacing[4],
}
