import React, { FC, useState } from "react"
import { View, ViewStyle, TextStyle, Animated, FlatList } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, TextField } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import { theme } from "../../theme/default"
import { useTheme } from "react-native-paper"

import { metrics } from "../../theme/metrics"
import { translate } from "../../i18n"

export const SearchScreen: FC<StackScreenProps<NavigatorParamList, "search">> = observer(
  ({ navigation }) => {
    const { colors } = useTheme()

    const DATA = [
      {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
        title: "Ders Detay",
        title2: "Dersler",
      },
      {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63a",
        title: "Yemek Menüsü",
        title2: "Üniversite",
      },
      {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Kalan Bakiye",
        title2: "Hesap",
      },
      {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ta",
        title: "Kampüs Haritası",
        title2: "Üniversite",
      },
      {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97ff63",
        title: "Kampanyalar",
      },
      {
        id: "58694a0f-3da1-471f-bd96-145571e29d372",
        title: "Acil Yardım",
        title2: "Güvenlik",
      },
    ]

    const [state] = useState(new Animated.Value(100))

    React.useEffect(() => {
      Animated.timing(state, {
        toValue: 80,
        duration: 500,
        useNativeDriver: false,
      }).start()
    })
    const goBack = () => {
      navigation.goBack()
      Animated.timing(state, {
        toValue: 100,
        duration: 200,
        useNativeDriver: false,
      }).start()
    }

    const renderItem = ({ item }) => (
      <View style={MENU_ITEM}>
        <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>{item.title}</Text>
        {item.title2 ? (
          <Text style={{ ...MENU_ITEM_TITLE, color: color.line }}>{item.title2}</Text>
        ) : undefined}
      </View>
    )
    return (
      <View testID="SearchScreen" style={FULL}>
        <Header titleStyle={theme.headerWelcome}>
          <View style={HEADER_CONTENT}>
            <Text style={HEADER_TEXT}>{translate("whatAreYouLookingFor")}</Text>
            <View style={HEADER_FORM_CONTAINER}>
              <Animated.View
                style={{
                  width: state.interpolate({
                    inputRange: [0, 100],
                    outputRange: ["0%", "100%"],
                  }),
                }}
              >
                <TextField
                  autoFocus
                  placeholder={translate("whatYouWannaDo")}
                  inputStyle={SEARCH_INPUT_STYLE}
                  style={SEARCH_INPUT_CONTAINER}
                />
              </Animated.View>
              <View style={{ width: "20%" }}>
                <Text onPress={() => goBack()} style={HEADER_FORM_BUTTON}>
                  {translate("cancel")}
                </Text>
              </View>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} style={CONTAINER} backgroundColor={color.transparent}>
          <FlatList
            data={DATA}
            renderItem={renderItem}
            contentContainerStyle={MENU_CONTAINER}
            keyExtractor={(item) => item.id}
          />
        </Screen>
      </View>
    )
  },
)

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}

const SEARCH_INPUT_CONTAINER: ViewStyle = {}
const SEARCH_INPUT_STYLE: TextStyle = {
  borderRadius: 20,
  fontSize: 16,
  paddingHorizontal: metrics.spacing[3],
}
const HEADER_TEXT: TextStyle = {
  ...theme.headerText,
  textAlign: "center",
  marginBottom: metrics.spacing[4],
}
const HEADER_CONTENT: ViewStyle = {
  marginBottom: metrics.spacing[3],
}
const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  borderWidth: 1,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
}
const MENU_CONTAINER: ViewStyle = {
  flex: 1,
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 14,
  marginBottom: metrics.spacing[1],
}
const HEADER_FORM_CONTAINER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
  overflow: "hidden",
}
const HEADER_FORM_BUTTON: TextStyle = {
  color: color.white,
  textAlign: "center",
  marginLeft: 5,
}
