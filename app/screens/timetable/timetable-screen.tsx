import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle, Text, TextStyle, FlatList, TouchableOpacity } from "react-native"
import { Button, Header, Screen } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import CalendarStrip from "react-native-calendar-strip"
import moment from "moment"
import tr from "moment/locale/tr"
import en from "moment/locale/en-gb"
import de from "moment/locale/de"
import { color, typography } from "../../theme"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import { palette } from "../../theme/palette"
import TimeTableStore from "../../services/stores/TimeTableStore"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import Icon from "react-native-vector-icons/EvilIcons"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"
import AppStore from "../../services/stores/AppStore"

// export const LessonsScreen: FC<StackScreenProps<NavigatorParamList, "welcome">> = observer(
//   function LessonsScreen({ navigation }) {

export const TimetableScreen: FC<StackScreenProps<NavigatorParamList, "TimeTable">> = observer(
  function TimetableScreen({ navigation }) {
    const { colors } = useTheme()
    const [locale, setLocale] = useState({
      tr: { name: "tr", config: tr },
      en: { name: "en", config: en },
      de: { name: "de", config: de },
    })
    useEffect(() => {
      const timeTable = TimeTableStore.get(
        moment().format("YYYY-MM-") + moment(TimeTableStore.selectedDate).format("DD"),
        moment().format("YYYY-MM-") + moment(TimeTableStore.selectedDate).format("DD"),
      )
      TimeTableStore.set(timeTable)
    }, [])

    const selectedDate = function (date) {
      TimeTableStore.setSelectedDate(date.toDate())
      const timeTable = TimeTableStore.get(
        moment().format("YYYY-") +
          moment(TimeTableStore.selectedDate).format("MM") +
          "-" +
          moment(TimeTableStore.selectedDate).format("DD") +
          "",
        moment().format("YYYY-") +
          moment(TimeTableStore.selectedDate).format("MM") +
          "-" +
          moment(TimeTableStore.selectedDate).format("DD") +
          "",
      )
      TimeTableStore.set(timeTable)
    }
    const renderItem = ({ item }) => {
      return (
        <View style={MENU_ITEM}>
          <View style={MENU_ITEM_TITLE_CONTAINER}>
            <Text
              style={{
                ...MENU_ITEM_TITLE,
                color: colors.primary,
              }}
            >
              {item.DersAd}
            </Text>
          </View>
          <View style={MENU_ITEM_CONTENT}>
            <View style={{ flex: 2 }}>
              <View style={MENU_ITEM_SUB}>
                <Icon name="tag" size={25} color={colors.primary} />
                <Text style={{ ...MENU_ITEM_TEXT, color: color.line }}>{item.DersKod}</Text>
              </View>
              <View style={MENU_ITEM_SUB}>
                <Icon name="archive" size={25} color={colors.primary} />
                <Text style={{ ...MENU_ITEM_TEXT, color: color.line }}>{item.GrupAd}</Text>
              </View>
              <View style={MENU_ITEM_SUB}>
                <Icon name="pencil" size={25} color={colors.primary} />
                <Text style={{ ...MENU_ITEM_TEXT, color: color.line }}>{item.Mekan.MekanAd}</Text>
              </View>
              <View style={MENU_ITEM_SUB}>
                <Icon name="clock" size={25} color={colors.primary} />
                <Text style={{ ...MENU_ITEM_TEXT, color: color.line }}>
                  {moment(item.Baslangic).format("H:mm")} - {moment(item.Bitis).format("H:mm")}
                </Text>
              </View>
            </View>
            <View style={MENU_ITEM_TIME_CONTAINER}>
              <Text style={{ ...MENU_ITEM_TIME, color: colors.primary }}>{item.Saat}</Text>
              <Text style={{ ...MENU_ITEM_TIME, color: colors.primary }}>{item.Sure}</Text>
            </View>
          </View>
        </View>
      )
    }
    function Calendar() {
      return (
        <CalendarStrip
          locale={locale[AppStore.activeLanguage]}
          selectedDate={TimeTableStore.selectedDate}
          scrollable={true}
          onDateSelected={async (date) => {
            selectedDate(date)
          }}
          calendarAnimation={{ type: "sequence", duration: 30 }}
          daySelectionAnimation={{
            type: "border",
            duration: 200,
            borderWidth: 1,
            borderHighlightColor: color.second,
          }}
          style={{
            backgroundColor: color.transparent,
            height: 90,
            paddingLeft: 20,
            paddingRight: 20,
          }}
          calendarHeaderStyle={{
            color: color.white,
            fontSize: 20,
            textTransform: "uppercase",
            margin: 0,
            padding: 0,
          }}
          calendarColor={color.white}
          dateNumberStyle={{ color: color.white }}
          dateNameStyle={{ color: color.white }}
          highlightDateNumberStyle={{ color: color.white }}
          highlightDateNameStyle={{ color: color.white }}
          leftSelector={[]}
          rightSelector={[]}
        />
      )
    }
    return (
      <View style={{ flex: 1 }}>
        <Header titleStyle={theme.headerWelcome}>
          <Text style={theme.headerText}>{translate("timetable")}</Text>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          {Calendar()}
          <View>
            {TimeTableStore.list.length > 0 ? (
              <FlatList
                data={TimeTableStore.list.toJSON()}
                renderItem={renderItem}
                style={{ marginBottom: 50 }}
                keyExtractor={(item, index) => "key" + index}
              />
            ) : (
              <View
                style={{
                  flexDirection: "row",
                  padding: metrics.spacing[3],
                  borderRadius: 10,
                  backgroundColor: color.white,
                  shadowOpacity: 0.04,
                  shadowRadius: 5.0,
                  borderColor: color.dim,
                  marginVertical: metrics.spacing[1],
                  marginHorizontal: metrics.spacing[1],
                  position: "relative",
                  overflow: "hidden",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Icon name="question" size={35} color="#4F8EF7" />
                <Text
                  style={{
                    color: color.text,
                    fontSize: 16,
                    fontFamily: typography.primary,
                  }}
                >
                  {translate("noCourse")}
                </Text>
              </View>
            )}
          </View>
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
  flex: 1,
}
const MENU_ITEM_CONTENT: ViewStyle = { flexDirection: "row", marginTop: metrics.spacing[2] }

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE_CONTAINER: ViewStyle = {
  borderBottomColor: palette.grey200,
  borderBottomWidth: 1,
  paddingBottom: metrics.spacing[1],
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 16,
}
const MENU_ITEM_TEXT: TextStyle = {
  ...MENU_ITEM_TITLE,
  color: color.line,
  fontSize: 13,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
  marginBottom: metrics.spacing[1],
}
const MENU_ITEM_TIME_CONTAINER: ViewStyle = {
  flex: 1,
  alignItems: "center",
  justifyContent: "space-evenly",
}
const MENU_ITEM_TIME: TextStyle = {
  fontSize: 18,
  fontFamily: typography.primary,
}
