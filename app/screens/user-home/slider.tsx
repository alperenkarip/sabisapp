import React, { FC, useEffect } from "react"
import { View, Text, TouchableOpacity, ViewStyle, FlatList } from "react-native"
import { SceneMap, TabBar, TabView } from "react-native-tab-view"
import { color } from "../../theme"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { observer } from "mobx-react-lite"
import { useNavigation } from "@react-navigation/native"
import FoodMenuStore from "../../services/stores/FoodMenuStore"
import { useTheme } from "react-native-paper"
import AnnouncementsStore from "../../services/stores/AnnouncementsStore"
import { translate } from "../../i18n"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"

export const slider = observer(function slider() {
  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([{ key: "first" }, { key: "second" }])

  const FirstRoute = () => {
    const navigation = useNavigation()
    const { colors } = useTheme()
    return (
      <View
        style={{
          backgroundColor: colors.third,
          flex: 1,
          borderRadius: 15,
          paddingHorizontal: metrics.spacing[3],
          paddingTop: metrics.spacing[3],
          paddingBottom: metrics.spacing[3],
        }}
      >
        <Text
          style={{
            ...theme.textStyle,
            color: color.white,
            fontSize: 18,
            marginBottom: metrics.spacing[1],
          }}
        >
          {translate("news")}
        </Text>

        <FlatList
          data={AnnouncementsStore.news}
          ItemSeparatorComponent={() => (
            <Text
              style={{ height: 1, marginVertical: 8, backgroundColor: color.palette.blueGrey300 }}
            ></Text>
          )}
          renderItem={(data) => {
            return (
              <View style={{}}>
                <Text style={{ ...FOODMENU_ITEM, fontSize: 16, color: color.white }}>
                  {data.item.Baslik}
                </Text>
              </View>
            )
          }}
          keyExtractor={(item, index) => "key" + index}
        />
      </View>
    )
  }

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      tabStyle={{
        width: "auto",
        padding: metrics.spacing[1],
      }}
      contentContainerStyle={{
        justifyContent: "center",
      }}
      bounces={true}
      renderIcon={({ route, focused }) => {
        let iconColor = focused ? color.second : color.dim
        return <Icon name="moon-full" size={9} color={iconColor} />
      }}
      indicatorStyle={{
        backgroundColor: color.transparent,
      }}
      style={{
        backgroundColor: color.transparent,
      }}
    />
  )

  const SecondRoute = () => {
    const navigation = useNavigation()
    const { colors } = useTheme()

    return (
      <View
        style={{
          backgroundColor: colors.third,
          flex: 1,
          borderRadius: 15,
          paddingHorizontal: metrics.spacing[3],
          paddingTop: metrics.spacing[3],
          paddingBottom: metrics.spacing[3],
        }}
      >
        <Text
          style={{
            ...theme.textStyle,
            color: color.white,
            fontSize: 18,
            marginBottom: metrics.spacing[1],
          }}
        >
          {translate("events")}
        </Text>

        <FlatList
          data={AnnouncementsStore.events}
          ItemSeparatorComponent={() => (
            <Text
              style={{ height: 1, marginVertical: 8, backgroundColor: color.palette.blueGrey300 }}
            ></Text>
          )}
          renderItem={(data) => {
            return (
              <View style={{}}>
                <Text style={{ ...FOODMENU_ITEM, fontSize: 16, color: color.white }}>
                  {data.item.EtkinlikAdi}
                </Text>
              </View>
            )
          }}
          keyExtractor={(item, index) => "key" + index}
        />
      </View>
    )
  }
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  })
  return (
    <TabView
      tabBarPosition="bottom"
      renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
    />
  )
})

const MENU_ITEM: ViewStyle = {
  borderColor: color.dim,
  position: "relative",
  overflow: "hidden",
  justifyContent: "center",
  flex: 1,
}
const FOODMENU_ITEM = {
  ...theme.textStyle,
}

export default slider
