import React, { FC, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { Dimensions, FlatList, View, ViewStyle, Animated } from "react-native"
import { Header, Screen, Text } from "../../components"
import { color, typography } from "../../theme"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import SlidingUpPanel from "rn-sliding-up-panel"
import Icon2 from "react-native-vector-icons/MaterialIcons"
import Slider from "./slider"
import { palette } from "../../theme/palette"
import FoodMenuStore from "../../services/stores/FoodMenuStore"
import moment from "moment"
import FoodCardStore from "../../services/stores/FoodCardStore"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"
import AnnouncementsStore from "../../services/stores/AnnouncementsStore"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"

const { height } = Dimensions.get("window")
export const UserHomeScreen: FC<StackScreenProps<NavigatorParamList, "Card">> = observer(
  function UserHomeScreen({ navigation }) {
    const { colors } = useTheme()

    useEffect(() => {
      AnnouncementsStore.getNews()
      AnnouncementsStore.getEvents()
    }, [])

    // useEffect(() => {
    //   const date = moment().toDate()
    //   FoodMenuStore.get(date.getDate(), date.getMonth() + 1, date.getFullYear())
    // }, [])

    // useEffect(() => {
    //   FoodCardStore.get()
    // }, [])

    const _draggedValue = new Animated.Value(100)

    // Pull in navigation via hook
    // const navigation = useNavigation()
    // const renderItem = ({ item }) => (
    //   <View style={SHORTCUT_ITEM}>
    //     <Text style={SHORTCUT_SUBTITLE}>{item.title2}</Text>
    //     <Text style={{ ...SHORTCUT_TITLE, color: colors.primary }}>{item.title}</Text>
    //   </View>
    // )
    const renderItem2 = ({ item }) => {
      return (
        <View style={SHORTCUT_ITEM}>
          <Text style={{ ...SHORTCUT_TITLE, color: colors.primary }}>{item.title}</Text>
          <Text style={SHORTCUT_SUBTITLE}>{item.title2}</Text>
        </View>
      )
    }
    // const renderItem3 = ({ item }) => {
    //   return (
    //     <View style={SHORTCUT_ITEM}>
    //       <Text style={{ ...SHORTCUT_TITLE, color: colors.primary }}>{item.title}</Text>
    //       <Text style={SHORTCUT_SUBTITLE}>{item.title2}</Text>
    //     </View>
    //   )
    // }

    return (
      <View style={{ flex: 1 }}>
        <Header childrenStyle={{ flex: 1 }} style={{ flex: 1 }} titleStyle={theme.headerWelcome}>
          <View style={{ flex: 1, marginTop: metrics.spacing[2] }}>
            <Slider />
          </View>
        </Header>
        <View style={{ flex: 1 }}>
          <SlidingUpPanel
            draggableRange={{ top: height / 1.24, bottom: height / 2.2 }}
            animatedValue={_draggedValue}
            showBackdrop={false}
          >
            <View style={SWIPE_CONTENT_CONTAINER}>
              <View style={theme.alignCenter}>
                <Icon2 name="horizontal-rule" size={45} color={color.line} />
              </View>
              <Screen unsafe={true} style={ROOT} backgroundColor={color.transparent}>
                <View>
                  {/* <View style={LINK}>
                  <Text style={theme.titleText}>Kısayollarım</Text>
                  <Text style={{ ...LINK_BUTTON, color: colors.text }}>Düzenle</Text>
                </View>
                <FlatList
                  horizontal={true}
                  scrollEnabled={true}
                  data={DATA}
                  style={{
                    paddingHorizontal: metrics.spacing[3],
                  }}
                  renderItem={renderItem}
                  keyExtractor={(item) => item.id}
                /> */}
                  <View style={{ ...LINK, marginTop: metrics.spacing[0] }}>
                    <Text style={theme.titleText}>{translate("upcomingLessons")}</Text>
                    {/* <Text style={{ ...LINK_BUTTON, color: colors.text }}>Tümü</Text> */}
                  </View>
                  <FlatList
                    data={DATA2}
                    style={FLATLIST_STYLE}
                    renderItem={renderItem2}
                    keyExtractor={(item) => item.id}
                  />
                  {/* <View style={{ ...LINK, marginTop: metrics.spacing[5] }}>
                  <Text style={theme.titleText}>Kart İşlemleri</Text>
                  <Text style={{ ...LINK_BUTTON, color: colors.text }}>Tümü</Text>
                </View>
                {FoodCardStore.list > 0 ? (
                  <FlatList
                    data={FoodCardStore.list.toJSON()}
                    style={FLATLIST_STYLE}
                    renderItem={renderItem3}
                    keyExtractor={(item) => item.id}
                  />
                ) : (
                  <View style={{ ...SHORTCUT_ITEM, paddingHorizontal: metrics.spacing[3] }}>
                    <Text style={SHORTCUT_SUBTITLE}>Kart bilgisi yok</Text>
                  </View>
                )} */}
                </View>
              </Screen>
            </View>
          </SlidingUpPanel>
        </View>
      </View>
    )
  },
)

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Ders Programı",
    title2: "Dersler",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Yemek Menüsü",
    title2: "Üniversite",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Notlar",
    title2: "Dersler",
  },
  {
    id: "bd7acbea2-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Ders Programı",
    title2: "Dersler",
  },
  {
    id: "3ac68afc3-c605-48d3-a4f8-fbd91aa97f63",
    title: "Yemek Menüsü",
    title2: "Üniversite",
  },
  {
    id: "58694a0f4-3da1-471f-bd96-145571e29d72",
    title: "Notlar",
    title2: "Dersler",
  },
]
const DATA2 = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "MATH 101 - Matematik I",
    title2: "20 Ekim 2021, 12:05",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "PHYS 101 - Fizik I	",
    title2: "20 Ekim 2021, 12:05",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "CS 101 - Programlamaya Giriş I	",
    title2: "20 Ekim 2021, 12:05",
  },
]
const ROOT: ViewStyle = {}
const SHORTCUT_ITEM = {
  borderRadius: 5,
  padding: metrics.spacing[2],
  marginRight: metrics.spacing[2],
  backgroundColor: color.white,
}
const SHORTCUT_SUBTITLE = {
  color: color.line,
  fontSize: 14,
}
const SHORTCUT_TITLE = { fontFamily: typography.semiBold, fontSize: 14 }
const SWIPE_CONTENT_CONTAINER = {
  flex: 1,
  backgroundColor: palette.grey100,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
}
const LINK = {
  justifyContent: "space-between",
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: metrics.spacing[3],
  paddingBottom: metrics.spacing[3],
}
const LINK_BUTTON = {
  fontFamily: typography.primary,
  fontSize: 15,
}
const FLATLIST_STYLE = {
  paddingHorizontal: metrics.spacing[3],
  backgroundColor: color.white,
}
