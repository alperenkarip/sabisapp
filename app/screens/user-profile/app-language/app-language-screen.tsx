import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import Icon from "react-native-vector-icons/EvilIcons"
import { StackScreenProps } from "@react-navigation/stack"
import { useEffect } from "react"
import { useTheme } from "react-native-paper"
import { metrics } from "../../../theme/metrics"
import { theme } from "../../../theme/default"
import { color, typography } from "../../../theme"
import DropDownPicker from "react-native-dropdown-picker"
import AppStore from "../../../services/stores/AppStore"
import { NavigatorParamList } from "../../../navigators"
import * as storage from "../../../utils/storage"
import UserStore from "../../../services/stores/UserStore"
import { translate } from "../../../i18n"

export const AppLanguageScreen: FC<StackScreenProps<NavigatorParamList, "AppLanguage">> = observer(
  function AppLanguageScreen({ navigation }) {
    const { colors } = useTheme()
    const [items, setItems] = useState([])
    const [activeLanguage, setActiveLanguage] = useState(AppStore.activeLanguage)
    const [open, setOpen] = useState(false)

    useEffect(() => {
      storage.getUser(UserStore.userInfo.id).then((user) => {
        user.activeLanguage = activeLanguage
        AppStore.setLanguage(activeLanguage)
        storage.mergeUsers(user)
        console.log(AppStore.activeLanguage)
        console.log(user)
      })
    })

    return (
      <View style={{ flex: 1 }}>
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              style={{ zIndex: 3, elevation: 3, width: 20 }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={40}
                color="#fff"
                style={{ marginLeft: -metrics.spacing[3] }}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerText }}>{translate("languageSelection")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <View style={{ flex: 1, height: 60 }}>
            <DropDownPicker
              open={open}
              value={activeLanguage}
              items={[
                { label: translate("tr"), value: "tr" },
                { label: translate("de"), value: "de" },
                { label: translate("en"), value: "en" },
              ]}
              setOpen={setOpen}
              onSelectItem={(language) => {
                console.log(language)
                AppStore.setLanguage(language.value)
              }}
              setValue={setActiveLanguage}
              setItems={setItems}
              zIndex={9999}
              style={{
                borderRadius: 20,
                borderWidth: 0,
                height: 44,
              }}
              dropDownContainerStyle={{
                borderWidth: 0,
              }}
              textStyle={{ color: color.text }}
              selectedItemLabelStyle={{
                color: colors.primary,
              }}
              customItemContainerStyle={{
                borderWidth: 0,
                borderColor: "red",
              }}
              customItemLabelStyle={{
                fontFamily: typography.primary,
              }}
              placeholder={translate("languageSelection")}
              placeholderStyle={{
                fontSize: 16,
                minHeight: 10,
                backgroundColor: color.palette.white,
                borderColor: colors.primary,
                color: color.palette.blueGrey300,
                fontFamily: typography.primary,
              }}
            />
          </View>
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
}
