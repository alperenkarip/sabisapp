import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { TouchableOpacity, View, ViewStyle, Text, FlatList } from "react-native"
import { Header, Screen } from "../../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../../theme"
import { useTheme } from "react-native-paper"
import { metrics } from "../../../theme/metrics"
import { theme } from "../../../theme/default"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../../navigators"
import Icon from "react-native-vector-icons/EvilIcons"
import { navigationRef } from "../../../navigators/navigation-utilities"
import * as storage from "../../../utils/storage"
import UserStore from "../../../services/stores/UserStore"
import AppStore from "../../../services/stores/AppStore"
import { translate } from "../../../i18n"

export const ShortcutsScreen: FC<StackScreenProps<NavigatorParamList, "Shortcuts">> = observer(
  function ShortcutsScreen({ navigation }) {
    const { colors } = useTheme()

    useEffect(() => {
      async function doAsync() {
        const user = await storage.getUser(UserStore.userInfo.id)
        AppStore.setShortcuts(user.shortcuts)
        console.log(user)
      }
      doAsync()
    }, [])

    // kısayol işlemlerinde çalışır
    useEffect(() => {
      // MERGE İŞLEMİ YAPILACAK
      storage.getUser(UserStore.userInfo.id).then((data) => {
        data.shortcuts = AppStore.shortcuts
        storage.mergeUsers(data)
      })
    })

    // useEffect(() => {
    //   const navigationState = navigationRef.current?.getRootState()

    //   navigationState.routes[0].state.routes.forEach((route) => {
    //     if (route.state) {
    //       route.state.routeNames.forEach((state) => {
    //         result[state] = state
    //       })
    //     }
    //     result[route.name] = route.name
    //   })
    //   console.log(result)
    //   console.log(JSON.stringify(result))
    // }, [navigationRef])

    return (
      <View style={{ flex: 1 }}>
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              style={{ zIndex: 3, elevation: 3, width: 20 }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={40}
                color="#fff"
                style={{ marginLeft: -metrics.spacing[3] }}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerText }}>{translate("shortcuts")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <FlatList
            horizontal={false}
            scrollEnabled={true}
            data={AppStore.shortcuts.slice().sort((x, y) => Number(y.active) - Number(x.active))}
            style={{
              paddingHorizontal: metrics.spacing[3],
            }}
            renderItem={(data) => {
              return (
                <TouchableOpacity
                  style={{
                    borderRadius: 5,
                    padding: metrics.spacing[2],
                    marginVertical: metrics.spacing[1],
                    backgroundColor: color.white,
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                  onPress={() => {
                    const { name, routeName, active, icon } = data.item
                    const update = AppStore.shortcuts.toJSON().filter((item) => item.name !== name)
                    AppStore.setShortcuts([
                      ...update,
                      { name: name, routeName: routeName, icon: icon, active: !active },
                    ])

                    AppStore.setShortcuts([
                      ...update,
                      { name: name, routeName: routeName, icon: icon, active: !active },
                    ])
                  }}
                >
                  <Text style={{ color: colors.text }}>{translate(data.item.name)}</Text>
                  <Icon
                    name="check"
                    size={21}
                    color={colors.primary}
                    style={{ display: data.item.active ? "flex" : "none" }}
                  />
                </TouchableOpacity>
              )
            }}
            keyExtractor={(item, index) => index + item.name}
          />
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
}
