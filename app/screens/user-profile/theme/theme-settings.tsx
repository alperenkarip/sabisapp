import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { TouchableOpacity, View, ViewStyle, Text } from "react-native"
import { Header, Screen } from "../../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../../theme"
import { useTheme } from "react-native-paper"
import { metrics } from "../../../theme/metrics"
import { theme } from "../../../theme/default"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../../navigators"
import Icon from "react-native-vector-icons/EvilIcons"
import DropDownPicker from "react-native-dropdown-picker"
import AppStore from "../../../services/stores/AppStore"
import * as storage from "../../../utils/storage"
import UserStore from "../../../services/stores/UserStore"
import { translate } from "../../../i18n"

export const ThemeSettings: FC<StackScreenProps<NavigatorParamList, "ThemeSettings">> = observer(
  function ThemeSettings({ navigation }) {
    const { colors } = useTheme()
    const [open, setOpen] = useState(false)
    const [activeTheme, setActiveTheme] = useState(AppStore.activeTheme)
    const [items, setItems] = useState([])

    useEffect(() => {
      storage.getUser(UserStore.userInfo.id).then((user) => {
        user.activeTheme = activeTheme
        AppStore.setTheme(activeTheme)
        storage.mergeUsers(user)
        console.log(AppStore.activeTheme)
        console.log(user)
      })
    })

    return (
      <View style={{ flex: 1 }}>
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              style={{ zIndex: 3, elevation: 3, width: 20 }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={40}
                color="#fff"
                style={{ marginLeft: -metrics.spacing[3] }}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerText }}>{translate("display")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <View style={{ flex: 1, height: 60 }}>
            <DropDownPicker
              open={open}
              value={activeTheme}
              items={[
                { label: "Sakarya Üniversitesi", value: "sau" },
                { label: "Tokyo", value: "tokyo" },
                { label: "Berlin", value: "berlin" },
              ]}
              setOpen={setOpen}
              onSelectItem={(theme) => {
                console.log(theme)
                AppStore.setTheme(theme.value)
              }}
              setValue={setActiveTheme}
              setItems={setItems}
              zIndex={9999}
              style={{
                borderRadius: 20,
                borderWidth: 0,
                height: 44,
              }}
              dropDownContainerStyle={{
                borderWidth: 0,
              }}
              textStyle={{ color: color.text }}
              selectedItemLabelStyle={{
                color: colors.primary,
              }}
              customItemContainerStyle={{
                borderWidth: 0,
                borderColor: "red",
              }}
              customItemLabelStyle={{
                fontFamily: typography.primary,
              }}
              placeholder="Tema seçin"
              placeholderStyle={{
                fontSize: 16,
                minHeight: 10,
                backgroundColor: color.palette.white,
                borderColor: colors.primary,
                color: color.palette.blueGrey300,
                fontFamily: typography.primary,
              }}
            />
          </View>
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
}
