import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../components"
import { color } from "../../theme"
import { metrics } from "../../theme/metrics"
import { theme } from "../../theme/default"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { useTheme } from "react-native-paper"
import { translate } from "../../i18n"

export const UserProfileScreen: FC<StackScreenProps<NavigatorParamList, "Card">> = observer(
  function UserProfileScreen({ navigation }) {
    const { colors } = useTheme()

    return (
      <View style={{ flex: 1 }}>
        <Header style={{ paddingBottom: metrics.spacing[3] }} titleStyle={theme.headerWelcome}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View style={{ flex: 1, marginLeft: -20 }}>
              <Text style={{ ...theme.headerText }}> {translate("profile")}</Text>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} preset="fixed" style={ROOT} backgroundColor={color.transparent}>
          <TouchableOpacity onPress={() => {}}>
            <View style={MENU_ITEM}>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {translate("profile")}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      fontSize: 13,
                    }}
                  >
                    {translate("profileDescription")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ThemeSettings")
            }}
          >
            <View style={MENU_ITEM}>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {translate("display")}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      fontSize: 13,
                    }}
                  >
                    {translate("displayDescription")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Shortcuts")
            }}
          >
            <View style={MENU_ITEM}>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {translate("shortcuts")}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      fontSize: 13,
                    }}
                  >
                    {translate("shortcutsDescription")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("AppLanguage")
            }}
          >
            <View style={MENU_ITEM}>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {translate("language")}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      fontSize: 13,
                    }}
                  >
                    {translate("languageDescription")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <View style={MENU_ITEM}>
              <Text style={{ ...MENU_ITEM_TITLE, color: colors.primary }}>
                {translate("logOut")}
              </Text>
              <View style={{ flexDirection: "row", marginTop: metrics.spacing[1] }}>
                <View style={{ ...MENU_ITEM_SUB, paddingRight: metrics.spacing[1], flex: 1 }}>
                  <Text
                    style={{
                      ...MENU_ITEM_TITLE,
                      color: color.line,
                      fontSize: 13,
                    }}
                  >
                    {translate("logoutDescription")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Screen>
      </View>
    )
  },
)

const ROOT: ViewStyle = {
  paddingHorizontal: metrics.spacing[2],
  flex: 1,
}

const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  shadowOpacity: 0.04,
  shadowRadius: 5.0,
  borderColor: color.dim,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
  paddingVertical: metrics.spacing[2],
  paddingHorizontal: metrics.spacing[2],
  position: "relative",
  overflow: "hidden",
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 14,
}
const MENU_ITEM_SUB: TextStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  textAlign: "left",
  alignItems: "center",
  flex: 1,
}
