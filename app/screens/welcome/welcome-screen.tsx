import React, { FC, useEffect, useState } from "react"
import { View, ViewStyle, TextStyle, FlatList, TouchableOpacity } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, TextField } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import { theme } from "../../theme/default"
import { metrics } from "../../theme/metrics"
import Icon from "react-native-vector-icons/EvilIcons"
import { useTheme } from "react-native-paper"
import * as storage from "../../utils/storage"
import UserStore from "../../services/stores/UserStore"
import AppStore from "../../services/stores/AppStore"
import { translate } from "../../i18n"
import { notifications } from "../../components/notification-manager/notifications"

export const WelcomeScreen: FC<StackScreenProps<NavigatorParamList, "Welcome">> = observer(
  ({ navigation }) => {
    const { colors } = useTheme()
    // const [shortcuts, setShortcuts] = useState([])

    // useEffect(() => {
    //   async function doAsync() {
    //     const user = await storage.getUser(UserStore.userInfo.id)
    //   }
    //   doAsync()
    // }, [])

    // useEffect(() => {
    //   async function doAsync() {
    //     const userInfo = UserStore.userInfo.toJSON()
    //     const getStorageUsers = await storage.load("users")
    //     let isData = false
    //     getStorageUsers.map((user) => {
    //       if (user.id == userInfo.id) {
    //         isData = true
    //         AppStore.setShortcuts(user.shortcuts)
    //       }
    //     })

    //     if (!isData) {
    //       console.log("users storage boş, oluşturuldu")
    //       getStorageUsers.push({
    //         id: userInfo.id,
    //         name: userInfo.given_name,
    //         activeTheme: AppStore.activeTheme,
    //         shortcuts: AppStore.shortcuts.toJSON(),
    //       })
    //       storage.save("users", getStorageUsers)
    //     }
    //   }
    //   doAsync()
    // }, [])

    useEffect(() => {
      notifications.upComingLessons()
    }, [])

    useEffect(() => {
      async function doAsync() {
        const userInfo = UserStore.userInfo.toJSON()
        const getStorageUsers = await storage.load("users")
        let isData = false
        getStorageUsers.map((user) => {
          if (user.id == userInfo.id) {
            isData = true
            AppStore.setShortcuts(user.shortcuts)
            AppStore.setTheme(user.activeTheme)
            AppStore.setLanguage(user.activeLanguage)
            console.log(user)
            console.log("kullanıcı ayarları tanımlandı")
          }
        })

        if (!isData) {
          console.log("users storage boş, oluşturuldu")
          getStorageUsers.push({
            id: userInfo.id,
            name: userInfo.given_name,
            activeTheme: AppStore.activeTheme,
            shortcuts: AppStore.shortcuts.toJSON(),
          })
          storage.save("users", getStorageUsers)
        }
      }
      doAsync()
    }, [])

    const renderItem = ({ item }) => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate(item.routeName)
        }}
        style={{ ...MENU_ITEM, borderColor: colors.primary }}
      >
        <Icon name={item.icon} size={45} color={colors.primary} />
        <Text style={{ ...MENU_ITEM_TITLE, color: colors.text }}>{translate(item.name)}</Text>
      </TouchableOpacity>
    )

    return (
      <View testID="WelcomeScreen" style={FULL}>
        <Header titleStyle={theme.headerWelcome}>
          <View style={HEADER_CONTENT}>
            <Text style={HEADER_TEXT}>{translate("whatAreYouLookingFor")}</Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => navigation.navigate("Search")}>
                  <TextField
                    editable={false}
                    placeholder={translate("whatYouWannaDo")}
                    inputStyle={SEARCH_INPUT_STYLE}
                    style={SEARCH_INPUT_CONTAINER}
                    pointerEvents="none"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Header>
        <Screen unsafe={true} style={CONTAINER} backgroundColor={color.transparent}>
          <FlatList
            data={AppStore.shortcuts.filter((shortcut) => shortcut.active === true)}
            numColumns={2}
            renderItem={renderItem}
            contentContainerStyle={MENU_CONTAINER}
            keyExtractor={(item, index) => "key" + index}
          />
        </Screen>
      </View>
    )
  },
)

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
  flex: 1,
}

const SEARCH_INPUT_CONTAINER: ViewStyle = {}
const SEARCH_INPUT_STYLE: TextStyle = {
  borderRadius: 20,
  fontSize: 16,
  paddingHorizontal: metrics.spacing[3],
}
const HEADER_TEXT: TextStyle = {
  ...theme.headerText,
  textAlign: "center",
  marginBottom: metrics.spacing[4],
}
const HEADER_CONTENT: ViewStyle = {
  // marginTop: metrics.spacing[5],
  marginBottom: metrics.spacing[3],
}
const MENU_ITEM: ViewStyle = {
  borderRadius: 10,
  backgroundColor: color.white,
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  height: 120,
  marginVertical: metrics.spacing[1],
  marginHorizontal: metrics.spacing[1],
}
const MENU_CONTAINER: ViewStyle = {
  marginHorizontal: metrics.spacing[2],
}
const MENU_ITEM_TITLE: TextStyle = {
  ...theme.textStyle,
  fontSize: 14,
  marginTop: metrics.spacing[4],
}
