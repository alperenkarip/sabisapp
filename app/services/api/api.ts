import { ApisauceInstance, create } from "apisauce"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import UserStore from "../stores/UserStore"
import TimeTableStore from "../stores/TimeTableStore"
import { makeAutoObservable } from "mobx"
import AppStore from "../stores/AppStore"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  static apisauce: ApisauceInstance
  sauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
    makeAutoObservable(this)
    Api.setup()
    Api.apisauce.axiosInstance.interceptors.request.use(
      (config) => {
        // console.log('req');
        // console.log(config.url);
        AppStore.setLoading(true)
        return config
      },
      (error) => {
        Promise.reject(error)
      },
    )
    Api.apisauce.axiosInstance.interceptors.response.use(
      (config) => {
        // console.log('resp');
        // console.log(config.config.url);
        AppStore.setLoading(false)
        return config
      },
      (error) => {
        // console.log('hata')
        AppStore.setLoading(false)
        Promise.reject(error)
      },
    )
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  static setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: "https://apimobil.sabis.sakarya.edu.tr/",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
  }

  static token() {
    this.apisauce.setHeaders({
      Authorization: "Bearer " + UserStore.accessToken,
    })
  }
}
