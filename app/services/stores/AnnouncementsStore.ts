import { types, flow } from "mobx-state-tree"
import { Api } from "../api"
const announcements = types.model({
  title: types.string,
})
const events = types.model({
  EtkinlikAdi: types.string,
  EtkinlikGun: types.string,
  EtkinlikAy: types.string,
  EtkinlikSaat: types.string,
  EtkinlikYer: types.string,
})
const news = types.model({
  Baslik: types.string,
  Link: types.string,
})
const AnnouncementsStore = types
  .model({
    announcements: types.array(announcements),
    events: types.array(events),
    news: types.array(news),
  })
  .actions((self) => ({
    getNews: flow(function* () {
      try {
        Api.token()
        const response = yield Api.apisauce.get(
          "https://haber.sakarya.edu.tr/json/uygulama-haber.php",
        )
        self.news = response.data
      } catch (error) {
        console.log(error)
      }
    }),
    getEvents: flow(function* () {
      try {
        Api.token()
        const response = yield Api.apisauce.get(
          "https://etkinlik.sakarya.edu.tr/json/uygulama-etkinlik.php",
        )
        self.events = response.data
      } catch (error) {
        console.log(error)
      }
    }),
  }))
  .create({
    announcements: [],
    events: [],
    news: [],
  })

export default AnnouncementsStore
