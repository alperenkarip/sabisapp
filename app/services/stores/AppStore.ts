import { types, flow } from "mobx-state-tree"
import { color } from "../../theme"
const theme = types.model({
  colors: types.model({
    primary: types.string,

    subTextColor: types.string,
    third: types.string,
    accent: types.string,

    background: types.string,
    text: types.string,
  }),
})
const themes = types.model({
  sau: theme,
  tokyo: theme,
  berlin: theme,
})
const shortcut = types.model({
  name: types.string,
  routeName: types.string,
  icon: types.string,
  active: types.boolean,
})
const AppStore = types
  .model({
    loading: types.boolean,
    activeTheme: types.maybeNull(types.string),
    activeLanguage: types.maybeNull(types.string),
    themes: themes,
    shortcuts: types.array(shortcut),
  })
  .actions((self) => ({
    setLoading: flow(function* (loading) {
      // console.log(self.loading + "________________")
      self.loading = loading
    }),
    getTheme: () => {
      return self.themes[self.activeTheme]
    },
    setTheme: (theme) => {
      self.activeTheme = theme
    },
    setLanguage: (language) => {
      self.activeLanguage = language
    },
    setShortcuts: (shortcuts) => {
      self.shortcuts = shortcuts
    },
  }))
  .create({
    loading: false,
    activeTheme: "sau",
    activeLanguage: "tr",
    themes: {
      sau: {
        colors: {
          primary: color.primary,

          subTextColor: color.palette.blueGrey100,
          third: color.palette.cyanAzure,
          accent: "#68DBFF",

          background: color.background,
          text: color.line,
        },
      },
      tokyo: {
        colors: {
          primary: color.palette.pink300,

          subTextColor: color.palette.blueGrey100,
          third: color.palette.cyanAzure,
          accent: color.palette.pink50,

          background: color.palette.pink50,
          text: color.text,
        },
      },
      berlin: {
        colors: {
          primary: color.palette.blueGrey300,

          subTextColor: color.palette.blueGrey100,
          third: color.palette.cyanAzure,
          accent: color.palette.blueGrey50,

          background: color.palette.blueGrey50,
          text: color.text,
        },
      },
    },
    shortcuts: [
      { name: "cardInfo", routeName: "Card", icon: "archive", active: true },
      { name: "displaySettings", routeName: "Display", icon: "archive", active: false },
      { name: "foodMenu", routeName: "FoodMenu", icon: "archive", active: false },
      { name: "lessons", routeName: "Lessons", icon: "archive", active: false },
      { name: "search", routeName: "Search", icon: "archive", active: false },
      { name: "personnelSearch", routeName: "SearchPerson", icon: "archive", active: false },
      { name: "shortcuts", routeName: "Shortcuts", icon: "archive", active: false },
      { name: "timetable", routeName: "TimeTable", icon: "archive", active: false },
      { name: "userScreen", routeName: "UserHome", icon: "archive", active: false },
      { name: "profileSettings", routeName: "UserProfile", icon: "archive", active: false },
    ],
  })

export default AppStore
