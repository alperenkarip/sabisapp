import { types, flow } from "mobx-state-tree"
import { Api } from "../api"
const action = types.union(
  types.model({
    balance: types.string,
    previousBalance: types.string,
    date: types.string,
    clientName: types.string,
  }),
)
const FoodCardStore = types
  .model({
    list: types.array(action),
    balance: types.integer,
  })
  .actions((self) => ({
    get: flow(function* () {
      try {
        Api.token()
        const response = yield Api.apisauce.get("api/v1/KartIslemleri/GetirKartBakiye")
        console.log(response.data)
        self.list = response.data.detailList
        self.balance = response.data.balance
      } catch (error) {
        console.log("foodcardstorehata")
        console.log(error)
      }
    }),
  }))
  .create({
    list: [],
    balance: 0,
  })

export default FoodCardStore
