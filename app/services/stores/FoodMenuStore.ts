import { types, flow, applySnapshot, cast } from "mobx-state-tree"
import { create } from "apisauce"

const Food = types.maybeNull(
  types.model({
    Kalori: types.maybeNull(types.number),
    Malzeme: types.string,
    YemekAd: types.string,
  }),
)
const FoodMenuStore = types
  .model({
    diet: types.boolean,
    date: types.union(types.Date, types.string),
    dateObj: types.model({
      day: types.maybeNull(types.number),
      month: types.number,
      year: types.number,
    }),
    foodList: types.maybeNull(types.array(Food)),
  })
  .actions((self) => ({
    get: flow(function* (day?, month?, year?) {
      const api = create({
        baseURL: "http://menu.sakarya.edu.tr/tr/Api/",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
      try {
        const response = yield api.get(
          "BaslangicBitisOzet?gun=" +
            (day ? day : self.dateObj.day) +
            "&ay=" +
            (month ? month : self.dateObj.month) +
            "&yil=" +
            (year ? year : self.dateObj.year) +
            "&diyet=" +
            self.diet,
        )
        if (response.ok) self.foodList = response.data
        // applySnapshot(self, { ...self, foodList: response.data })
        // self.foodList = response.data
      } catch (error) {
        console.log("foodmenustore")
        console.log(error)
        self.foodList = null
      }
    }),
    dateSet: (dateObj) => {
      self.dateObj = dateObj
    },
    dietSet: (diet) => {
      self.diet = diet
    },
  }))
  .create({
    diet: false,
    date: "",
    dateObj: {
      day: null,
      month: 1,
      year: 2022,
    },
    foodList: [{ Kalori: 0, Malzeme: "", YemekAd: "" }],
  })

export default FoodMenuStore
