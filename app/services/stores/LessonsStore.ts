import { types,flow, applySnapshot} from "mobx-state-tree"
import { Api } from "../api";
import UserStore from "./UserStore";
const test = new Api();
const ArrayPayList = types.model({
    AltDersPlanAnaID:  types.maybeNull(types.number),
    AltPayList:  types.maybeNull(types.string),
    CalismaTip:  types.number,
    CalismaTipAd: types.string,
    DersNot: types.maybeNull(types.model({
        AciklanmaTarihi: types.string,
        Not: types.number,
        NotString: types.string,
        ToplamKatki: types.number,
        ID: types.number
    })),
    NihaiYuzde: types.number,
    PayID: types.number,
    Sira: types.number,
    YuzdeString: types.string,
    Yuzdev2: types.union(types.maybeNull(types.string),types.number),
});

const LessonsStore = types.model({
    lessonAnnouncements:types.array(
        types.model({
            DersAd: types.string,
            DersGrupID: types.integer,
            ID: types.integer,
            Konu: types.string,
            Mesaj: types.string,
            Personel: types.string,
            Tarih: types.string
        })
    ),
    lessons:types.array(
        types.model({
        DersBasariPaket:types.model({
                AnketDoldurmusMu:types.boolean,
                BasariNot:types.model({
                    BagilOrtalama:types.number,
                    BasariNot:types.maybeNull(types.number),
                    EnumBasariNot:types.number,
                    MutlakOrtalama:types.number,
                    NotID:types.number,
                    TarihYayinlanma:types.string
                }),
                GrupID:types.number,
                PayList:types.array(ArrayPayList)
        }),
        DersBilgisi:types.model({
            AKTS: types.number,
            DSaat: types.number,
            DersAd: types.string,
            DersAdYabanci: types.string,
            DersKod: types.string,
            DersPlanAnaID: types.number,
            EnumDersBirimTipi: types.number,
            EnumDersSecimTipi: types.number,
            EnumKokDersTipi: types.number,
            USaat: types.number,
            UzemMi: types.boolean
        }),
        GrupBilgisi:types.model({
            Birim: types.maybeNull(types.string),
            DersProgramiList: types.maybeNull(types.string),
            EnumOgretimTur: types.number,
            Fakulte: types.maybeNull(types.string),
            GrupAd: types.string,
            GrupID: types.number,
            OgretimGorevlisiList: types.maybeNull(types.string),
        }),
        YazilmaBilgisi:types.model({
            DevamZorunluluguDisinda: types.boolean,
            EnumIptalSebebi: types.maybeNull(types.string),
            Iptal: types.boolean,
            YazilmaID: types.number,
            YazilmaTarihi: types.string
        })
    })),
}).actions(self => ({
    getAnnouncements: flow(function* (dersGrupID){
        Api.token();
        const response =  yield Api.apisauce.get("api/v1/Obis/GetirGrupDuyuru?DersGrupID="+dersGrupID)
        applySnapshot(self, {...self,lessonAnnouncements:response.data});
    }),
    getLessons: flow(function* (){
         Api.token();
         const response =  yield Api.apisauce.get("api/v1/Obis/GetirDersler?yil=2021&donem=1")
         applySnapshot(self, {lessons:response.data});
    })
}))
.create({
    lessonAnnouncements:[],
    lessons:[]
});

export default LessonsStore;