import { types,flow, detach} from "mobx-state-tree"
import { Api } from "../api";
const unit = types.array(types.model({
    birimAdi:types.string,
    birimAdiUzun:types.string,
    id:types.integer,
    ustBirimID:types.integer,
    value:types.integer,
    label:types.string,
}))
const personnel = types.model({
    Aciklama: types.maybeNull(types.string),
    Ad: types.maybeNull(types.string),
    BirimAd:types.maybeNull(types.string),
    BirimKodu:types.maybeNull(types.string),
    DahiliNo: types.maybeNull(types.string),
    EPosta: types.maybeNull(types.string),
    FKBirimID: types.maybeNull(types.integer),
    Favori: types.maybeNull(types.boolean),
    ID: types.maybeNull(types.integer),
    KullaniciAdi: types.maybeNull(types.string),
    Soyad: types.maybeNull(types.string),
    Unvan: types.maybeNull(types.string),
    Web: types.maybeNull(types.string)
})
const PersonnelSearchStore = types.model('PersonnelSearchStore',{
    units:unit,
    personnels:types.array(personnel)
}).actions(self => ({

    getUnits: flow(function* (){
        Api.token()
         const response =  yield Api.apisauce.get("api/v1/Rehber/GetirBirimList")
         response.data.map((d, i) => {
            d.value = d.id;
            d.label = d.birimAdi;
          });
          detach(self.units);
          self.units = response.data;
    }),
    search: flow(function* (personnelName,unitID){
        Api.token();
        const response =  yield Api.apisauce.get("api/v1/Rehber/Sorgula",{ara:personnelName,birimID:unitID})
        self.personnels = response.data;
    })
})).create({
    units:[],
    personnels:[]
});

export default PersonnelSearchStore;