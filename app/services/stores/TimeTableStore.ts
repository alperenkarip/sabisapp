import { types, flow, applySnapshot } from "mobx-state-tree"
import moment from "moment"
import { Api } from "../api"
const lesson = types.model({
  Baslangic: types.string,
  Bitis: types.string,
  DersAd: types.string,
  DersKod: types.string,
  GrupAd: types.string,
  GrupID: types.integer,
  Gun: types.integer,
  Mekan: types.model({
    BinaAd: types.maybeNull(types.string),
    BinaFoto: types.maybeNull(types.string),
    Foto: types.maybeNull(types.string),
    Kat: types.integer,
    MekanAd: types.string,
    MekanID: types.integer,
    MekanKod: types.string,
    MekanUzunAd: types.string,
  }),
  PersonelID: types.integer,
  ProgramID: types.integer,
  ProgramIDList: types.array(types.integer),
  Saat: types.integer,
  Sure: types.integer,
})
const TimeTableStore = types
  .model({
    list: types.array(lesson),
    selectedDate: types.Date,
  })
  .actions((self) => ({
    get: flow(function* (baslangic, bitis) {
      Api.token()
      const response = yield Api.apisauce.get(
        "api/v1/Obis/GetirDersProgram?yil=2021&donem=1&baslangic=" + baslangic + "&bitis=" + bitis,
      )
      //  applySnapshot(self, {...self,list:response.data});
      // self.list = response.data;
      return response.data
    }),
    set: flow(function* (data) {
      self.list = yield data
    }),
    setSelectedDate: flow(function* (date) {
      self.selectedDate = date
    }),
  }))
  .create({
    list: [],
    selectedDate: new Date(),
  })

export default TimeTableStore
