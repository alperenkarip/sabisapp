import { authorize } from "react-native-app-auth"
import { types, flow, applySnapshot } from "mobx-state-tree"
import { create } from "apisauce"
import AppStore from "./AppStore"

const UserStore = types
  .model("UserStore", {
    accessToken: types.union(types.string, types.boolean),
    userInfo: types.maybeNull(
      types.model({
        Number: types.string,
        PersonelId: types.union(types.string, types.undefined),
        StudentId: types.union(types.string, types.undefined),
        SocialNumber: types.string,
        email: types.string,
        family_name: types.string,
        given_name: types.string,
        id: types.string,
        name: types.string,
        picture: types.string,
        role: types.string,
        sub: types.string,
      }),
    ),
    waitUserInfoWithToken: types.maybeNull(types.string),
  })
  .actions((self) => ({
    afterCreate() {
      console.log(self.accessToken)
      console.log(self.userInfo)
    },
    Authorize: flow(function* (callback) {
      const res = yield authorize({
        issuer: "https://login.sabis.sakarya.edu.tr",
        clientId: "Sabis.Mobil",
        clientSecret: "a0e40328-2300-a881-ab2a-025165ce5c33",
        redirectUrl: "com.sabisapp://oauth",
        scopes: ["openid", "sabis.mobil.api", "profile", "roles"],
      })
      // loginStore.data.form.username = "B211602053";
      // loginStore.data.form.password = "1ulku.K1";
      self.waitUserInfoWithToken = res.accessToken

      callback()
    }),
    GetUserInfo: flow(function* () {
      const api = create({
        baseURL: "https://login.sabis.sakarya.edu.tr",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
          Authorization: "Bearer " + self.waitUserInfoWithToken,
        },
      })
      const response = yield api.get("/connect/userinfo")
      applySnapshot(self, { accessToken: self.waitUserInfoWithToken, userInfo: response.data })
      AppStore.setLoading(false)
    }),
  }))
  .create({
    userInfo: {
      Number: "",
      PersonelId: "",
      StudentId: "",
      SocialNumber: "",
      email: "",
      family_name: "",
      given_name: "",
      id: "",
      name: "",
      picture: "",
      role: "",
      sub: "",
    },
    accessToken: false,
    waitUserInfoWithToken: null,
  })

export default UserStore
