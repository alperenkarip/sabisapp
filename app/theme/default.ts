import {color} from "./color";
import { metrics } from "./metrics";
import { palette } from "./palette";
import {typography} from "./typography";

export const theme = {
  textStyle: {
    fontSize: 15,
    fontFamily: typography.semiBold,
  },
  headerWelcome:{
    fontSize:15,
    fontFamily:typography.extraBold,
    color:color.white
  },
  headerText:{
    fontSize:30,
    color:color.white,
    fontFamily:typography.extraBold,
    textAlign:'center'
  },
  headerTextWithSub:{
    textAlign:'center',
    fontSize:20,
    color:color.white,
    fontFamily:typography.extraBold,
  },
  titleText:{
    fontSize:16,
    color:color.text,
    fontFamily:typography.semiBold,
  },
  alignCenter:{ alignItems: "center" }
};

